![vital-5g-logo](https://www.vital5g.eu/wp-content/uploads/2020/12/vital-logo-web.png) 
 # Service LCM 


[VITAL-5G D2.1](https://www.vital5g.eu/wp-content/uploads/2022/01/VITAL5G_D2.1_Initial_NetApps_blueprints_and_Open_Repository_design_Final.pdf) 

*The Service LifeCycle Manager (Service LCM) is the component responsible for processing the lifecycle management requests to create, terminate, query and update vertical service instances. This module offers a REST-based north-bound interface (NBI) which can be accessed transparently through the VITAL-5G GUI or using external REST clients.*

## Software architecture
The following figure illustrates the software architecture of this module, highlighting the components currently supported

![vital-5g-catalogue-software-architecture](docs/vital-5g-service-lcm-software-architecture.png)

The source code is available in the [SRC](src/) folder of the repository. The code is structured in two different maven projects:
* *vital-5g-service-lcm-interfaces* : Containing the Java  based interfaces and and classes representing the information models of the instances.
* *vital-5g-service-lcm*: Containing the implementation of the rest controllers, the logic for instance management and driver for the interaction with the rest of the components of the platform.

## Deployment 

A docker-compose based deployment is detailed in [Readme](installation/)

## Folder structure
* [src](src/): Contains the source code of this module
* [API](API/): OpenAPI specification of the interfaces by this module and Postman collections
* [docs](docs/): Documentation of the module, including example descriptors using during the integration tests  

## License
This module has been developed by [Nextworks](www.nextworks.it) and licensed under the open source [Apache License v2.0](https://www.apache.org/licenses/LICENSE-2.0)
