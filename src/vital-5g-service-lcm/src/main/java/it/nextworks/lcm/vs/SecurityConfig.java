package it.nextworks.lcm.vs;

import it.nextworks.lcm.vs.keycloak.CustomKeycloakSpringBootConfigResolver;
import org.keycloak.adapters.KeycloakConfigResolver;
import org.keycloak.adapters.springboot.KeycloakSpringBootProperties;
import org.keycloak.adapters.springsecurity.KeycloakConfiguration;
import org.keycloak.adapters.springsecurity.KeycloakSecurityComponents;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.client.KeycloakClientRequestFactory;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;

@KeycloakConfiguration
class SecurityConfig extends KeycloakWebSecurityConfigurerAdapter {



    @Autowired
    public KeycloakClientRequestFactory keycloakClientRequestFactory;

    @Value("${auth.admin-role:Platform_admin}")
    private String adminRole;

    @Value("${auth.technician-role:Platform_technician}")
    private String technicianRole;

    @Value("${auth.netapp-developer-role:NetApp_Developer}")
    private String netAppDeveloperRole;

    @Value("${auth.vertical-service-provider-role:Vertical_Service_Provider}")
    private String vspRole;

    @Value("${auth.experimenter-role:Experimenter}")
    private String experimenterRole;

    @Value("${auth.testbed-admin-role:Testbed_Admin}")
    private String testbedAdminRole;

    @Value("${auth.platform-module-role:Platform_module}")
    private String platformModuleRole;

    @Value("${auth.site-infrastructure-manager-role:T&L_Site Infrastructure_Manager}")
    private String simRole;


    /**
     * Registers the KeycloakAuthenticationProvider with the authentication manager.
     */
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        KeycloakAuthenticationProvider keycloakAuthenticationProvider = keycloakAuthenticationProvider();

        auth.authenticationProvider(keycloakAuthenticationProvider);
    }

    /**
     * Provide a session authentication strategy bean which should be of type
     * RegisterSessionAuthenticationStrategy for public or confidential applications
     * and NullAuthenticatedSessionStrategy for bearer-only applications.
     */
    @Bean
    @Override
    protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
        return new RegisterSessionAuthenticationStrategy(new SessionRegistryImpl());
    }

    /**
     * Use properties in application.properties instead of keycloak.json
     */
    @Bean
    @Primary
    public KeycloakConfigResolver keycloakConfigResolver(KeycloakSpringBootProperties properties) {
        return new CustomKeycloakSpringBootConfigResolver(properties);
    }

    /**
     * Secure appropriate endpoints
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);
        http.csrf()
                .disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/v2/api-docs").permitAll()
                .antMatchers(HttpMethod.GET, "/portal/vslcm").hasAnyAuthority(adminRole, technicianRole, experimenterRole, vspRole)
                .antMatchers(HttpMethod.GET, "/portal/vslcm/").hasAnyAuthority(adminRole, technicianRole, experimenterRole, vspRole)
                .antMatchers(HttpMethod.POST, "/portal/vslcm").hasAnyAuthority(adminRole, technicianRole, experimenterRole, vspRole)
                .antMatchers(HttpMethod.POST, "/portal/vslcm/").hasAnyAuthority(adminRole, technicianRole, experimenterRole, vspRole)
                .antMatchers(HttpMethod.GET, "/portal/vslcm/{vsid}").hasAnyAuthority(adminRole,  technicianRole, experimenterRole, vspRole)
                .antMatchers(HttpMethod.POST, "/portal/vslcm/{vsid}/use").hasAnyAuthority(adminRole,  technicianRole, experimenterRole, vspRole, platformModuleRole)
                .antMatchers(HttpMethod.POST, "/portal/vslcm/{vsid}/release").hasAnyAuthority(adminRole,  technicianRole, experimenterRole, vspRole, platformModuleRole)

                .anyRequest().authenticated()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }
}