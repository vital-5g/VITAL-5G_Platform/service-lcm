package it.nextworks.lcm.vs.control;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import it.nextworks.catalogue.elements.*;
import it.nextworks.lcm.vs.VsLcmService;
import it.nextworks.lcm.vs.elements.InfrastructureMetricRecord;
import it.nextworks.lcm.vs.elements.VerticalServiceInstance;
import it.nextworks.lcm.vs.exceptions.FailedOperationException;
import it.nextworks.lcm.vs.exceptions.MalformattedElementException;
import it.nextworks.lcm.vs.exceptions.NotExistingEntityException;
import it.nextworks.lcm.vs.exceptions.UnAuthorizedRequestException;
import it.nextworks.lcm.vs.interfaces.VsLcmProviderInterface;
import it.nextworks.lcm.vs.manager.VsLcmManager;
import it.nextworks.lcm.vs.messages.OnboardRuntimeActionRequest;
import it.nextworks.lcm.vs.messages.ScaleVsNSRequest;
import it.nextworks.lcm.vs.messages.ScaleVsRequest;
import it.nextworks.lcm.vs.repo.VerticalServiceInstanceInfoRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;
import zmqpubsub.ZmqSubscriber;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.function.BiConsumer;

@Service
public class RuntimeActionService {
    private static final Logger log = LoggerFactory.getLogger(RuntimeActionService.class);

    @Value("${monitoring.address:localhost}")
    private String monitoringAddress;

    @Value("${monitoring.subscription_port:2054}")
    private String monitoringSubscriptionPort;


    private Map<UUID, VsLcmManager> vsManagers = new HashMap<>();

    private Map<UUID, RuntimeActionRecord> runtimeActionRecord= new HashMap<>();

    private Map<String, List<UUID>> topicRecords= new HashMap<>();


    private VsLcmProviderInterface vsLcmService;



    private boolean subscribed=false;



    public ZContext context = new ZContext();
    public ZMQ.Socket socket;
    public String bind;
    public Boolean isRunning;
    Thread.UncaughtExceptionHandler h = new Thread.UncaughtExceptionHandler() {
        public void uncaughtException(Thread th, Throwable ex) {
            System.out.println("subscriber stopped!");
        }
    };





    @PostConstruct
    private void configureRuntimeActionService(){
        log.debug("Configuring RuntimeActionService {} {}", monitoringAddress, monitoringSubscriptionPort);
        context = new ZContext();
        socket = context.createSocket(SocketType.SUB);

    }

    public void setVsLcmService(VsLcmProviderInterface vsLcmService) {
        this.vsLcmService = vsLcmService;
    }

    public UUID onboardRuntimeAction(UUID vsiId, VsLcmManager manager, OnboardRuntimeActionRequest request){
        log.debug("Configuring new RuntimeActoin for: {} {}");
        try {
            String metricId = request.getMetricId();
            VerticalServiceInstance verticalServiceInstance = vsLcmService.getVerticalServiceInstance(vsiId);
            InfrastructureMetricRecord record = verticalServiceInstance.getInfrastructureMetrics().stream().filter(mr -> mr.getMetricId().equals(metricId))
                    .findFirst().get();
            String metricTopic= record.getTopic();
            log.debug("Configuring infrastructure metric threshold trigger");
            log.debug("Topic: {} RuntimeAction: {}", metricTopic);
            ScaleVsNSRequest scaleNsVsReq =  (ScaleVsNSRequest)request.getScaleVsRequest();
            RuntimeNSScaleTarget target = new RuntimeNSScaleTarget(null,
                    scaleNsVsReq.getVnfdId(),
                    scaleNsVsReq.getCpus(),
                    scaleNsVsReq.getRam());
            RuntimeThresholdAction runtimeThresholdAction = new RuntimeThresholdAction(null,
                    null,
                    target,
                    metricId,
                    request.getMetricValue(),
                    RuntimeThresholdOperator.valueOf(request.getThresholdOperator().toString()));
            return configureThresholdInfrastructureMetricTrigger(vsiId, runtimeThresholdAction, metricTopic, null, manager);

        } catch (NotExistingEntityException e) {
            log.error("Error", e);
            return null;
        } catch (FailedOperationException e) {
            log.error("Error", e);
            return null;
        } catch (MalformattedElementException e) {
            log.error("Error", e);
            return null;
        } catch (UnAuthorizedRequestException e) {
            log.error("Error", e);
            return null;
        }
    }
    public UUID configureThresholdInfrastructureMetricTrigger(UUID vsiId, RuntimeAction runtimeAction, String metricTopic, InfrastructureMetric im, VsLcmManager vsLcmManager)
    throws FailedOperationException
    {
        log.debug("Configuring infrastructure metric threshold trigger");
        log.debug("Topic: {} RuntimeAction: {}", metricTopic, runtimeAction.getRuntimeActionId());

        UUID randomUUID = UUID.randomUUID();
        log.debug("Generated ID:{}", randomUUID.toString());


        log.debug("Configuring Infrastructure Metric Threshold");
        if(!subscribed){
            log.debug("Configuring Infrastructure Metric Threshold FIRST TIME");
            subscribed=true;
            subscribeFirstTime(metricTopic, this::incomingMonitoringDataHandler );
        }else subscribe(metricTopic);


        runtimeActionRecord.put(randomUUID, new RuntimeActionRecord(vsiId, randomUUID, runtimeAction, metricTopic));
        List<UUID> topicR;
        if(topicRecords.containsKey(metricTopic)){
            topicR = topicRecords.get(metricTopic);
        }else topicR = new ArrayList<>();
        topicR.add(randomUUID);
        topicRecords.put(metricTopic, topicR);
        vsManagers.put(randomUUID, vsLcmManager);
        return randomUUID;
    }


    @Async
    private void incomingMonitoringDataHandler(JsonObject jsonObject, String topicName){

        int metricValue = Math.round(jsonObject.get("value").getAsFloat());
        for(UUID runtimeRecordId : topicRecords.get(topicName)){
            RuntimeActionRecord record = runtimeActionRecord.get(runtimeRecordId);

            if(record!=null){
                RuntimeThresholdAction thAction = (RuntimeThresholdAction)record.getRuntimeAction();
                boolean conditionMet = false;
                if(thAction.getLogicOperator().equals(RuntimeThresholdOperator.GE))

                    conditionMet = metricValue>=Integer.parseInt(thAction.getMetricValue());
                else if(thAction.getLogicOperator().equals(RuntimeThresholdOperator.GT))
                    conditionMet = metricValue>Integer.parseInt(thAction.getMetricValue());
                else if(thAction.getLogicOperator().equals(RuntimeThresholdOperator.EQ))
                    conditionMet = metricValue==Integer.parseInt(thAction.getMetricValue());
                else if(thAction.getLogicOperator().equals( RuntimeThresholdOperator.LE))
                    conditionMet = metricValue<=Integer.parseInt(thAction.getMetricValue());
                else if(thAction.getLogicOperator().equals(RuntimeThresholdOperator.LT))
                    conditionMet = metricValue<Integer.parseInt(thAction.getMetricValue());

                if(conditionMet){
                    log.debug("RuntimeAction condition met");
                    if(!record.isActive()){
                        record.setActive(true);
                        RuntimeTarget target = thAction.getTarget();
                        if(target.getRuntimeTargetType().equals(RuntimeTargetType.NS_SCALE)){
                            log.debug("Triggering NS SCALE action");
                            RuntimeNSScaleTarget scaleTarget =  (RuntimeNSScaleTarget) target;
                            vsManagers.get(record.getRuntimeId()).scaleVs(scaleTarget.getVnfdId(), scaleTarget.getCpus().intValue(), scaleTarget.getRam().intValue());

                        }
                    }else log.debug("RuntimeAction is active. Ignoring");

                }else{
                    log.debug("RuntimeAction condition NOT met, DEACTIVATING");
                    record.setActive(false);
                }
                runtimeActionRecord.put(record.getRuntimeId(), record);
            }
        }

    }


    public void deactivateRuntimeAction(UUID runtimeId) throws FailedOperationException{
        log.debug("Deactivating RuntimeAction {}", runtimeId);
        InfrastructureMetric im = runtimeActionRecord.get(runtimeId).getInfrastructureMetric();
        String metricTopic = runtimeActionRecord.get(runtimeId).getMetricTopic();

        log.debug("unsubscribing Infrastructure Metric {}", metricTopic);
        unsubscribe(metricTopic);

        List<UUID> activeRecords  = topicRecords.get(metricTopic);
        activeRecords.remove(runtimeId);
        if(activeRecords.isEmpty()){
            topicRecords.remove(metricTopic);
        }else topicRecords.put(metricTopic, activeRecords);
        runtimeActionRecord.remove(runtimeId);
    }


    @Async
    public void subscribeFirstTime(String topic, BiConsumer<JsonObject, String> consumer) {
        this.socket.connect("tcp://" + monitoringAddress + ":" + monitoringSubscriptionPort);
        this.socket.subscribe(topic);
        this.isRunning = true;
        Thread t = new Thread(() -> {
            while(this.isRunning) {
                byte[] recv = this.socket.recv();

                try {
                    String receivedStringMessage = new String(recv);
                    String topicName = receivedStringMessage.substring(0, receivedStringMessage.indexOf(":"));
                    JsonObject jsonObject = (JsonObject) JsonParser.parseString(receivedStringMessage.substring(receivedStringMessage.indexOf(":") + 1));
                    consumer.accept(jsonObject, topicName);
                } catch (Exception var6) {
                    log.error("Error: parsing the received data (the data needs to be in JSON format", var6);
                }
            }

        });
        t.setUncaughtExceptionHandler(this.h);
        t.start();
    }

    public void subscribe(String topic) {
        this.socket.subscribe(topic);
    }

    public void unsubscribe(String topic) {
        this.socket.unsubscribe(topic);
    }

    public void stopSubscriber() {
        this.isRunning = false;
        this.socket.close();
    }


}
