package it.nextworks.lcm.vs.sbi.monitoring.interfaces;

import it.nextworks.catalogue.elements.ApplicationMetric;
import it.nextworks.catalogue.elements.InfrastructureMetric;
import it.nextworks.lcm.vs.exceptions.FailedOperationException;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface MonitoringProvider {



    MonitoringConfigResponse configureNetworkMonitoringMetrics(UUID vsiId, List<InfrastructureMetric> infrastructureMetrics, String nsInstanceName) throws FailedOperationException;

    MonitoringConfigResponse configureInfrastructureMonitoringMetrics(UUID vsiId, List<InfrastructureMetric> infrastructureMetrics, String nsInstanceName) throws FailedOperationException;

    MonitoringConfigResponse configureApplicationMonitoringMetrics(UUID vsiId, List<ApplicationMetric> applicationMetrics) throws FailedOperationException;

    void terminateInfrastructureMonitoringMetrics(List<String> topics) throws FailedOperationException;
    void terminateApplicationMonitoringMetrics(List<String> topics) throws FailedOperationException;
}
