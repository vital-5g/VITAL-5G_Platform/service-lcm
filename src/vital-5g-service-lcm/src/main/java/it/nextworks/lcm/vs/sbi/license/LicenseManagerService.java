package it.nextworks.lcm.vs.sbi.license;

import it.nextworks.catalogue.elements.NetAppBlueprint;
import it.nextworks.catalogue.elements.SoftwareLicense;
import it.nextworks.catalogue.exceptions.NotExistingEntityException;
import it.nextworks.lcm.license.messages.LcmOperationType;
import it.nextworks.lcm.license.messages.LicenseValidationRequest;
import it.nextworks.lcm.license.messages.LicenseValidationResult;
import it.nextworks.lcm.vs.exceptions.FailedOperationException;
import it.nextworks.lcm.vs.sbi.catalogue.Vital5GCatalogueService;
import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class LicenseManagerService {

    private static final Logger log = LoggerFactory.getLogger(LicenseManagerService.class);
    @Value("${licensemgr.url:http://localhost:8090}")
    private String licenseManagerUrl;

    @Autowired
    private KeycloakRestTemplate restTemplate;



    public boolean validateNetworkApplicationLicenses(List<NetAppBlueprint> netApps, Map<String, String> licenseKeys, UUID vsiId) throws FailedOperationException{
        log.debug("Received request to validate VS Network Application licenses");
        LicenseManagerRestClient restClient = new LicenseManagerRestClient(restTemplate, licenseManagerUrl);

        for(NetAppBlueprint bp: netApps){
            log.debug("Validating licenses for NetApp {}", bp.getNetAppPackageId());
            if(bp.getSoftwareLicenses()!=null && !bp.getSoftwareLicenses().isEmpty() ){
                for(SoftwareLicense sl: bp.getSoftwareLicenses()){
                    log.debug("Validating license {}", sl.getSoftwareLicenseId());
                    if(!sl.isOpenLicense()){
                        String licenseKey = null;
                        if(licenseKeys.containsKey(bp.getNetAppPackageId()))
                            licenseKey=licenseKeys.get(bp.getNetAppPackageId());
                        LicenseValidationRequest licReq = new LicenseValidationRequest(LcmOperationType.NETWORK_APPLICATION_INSTANTIATION, licenseKey, vsiId);
                        try {
                            LicenseValidationResult validationResult =
                                    restClient.validateLicense(bp.getNetAppPackageId(), licReq);
                            log.debug("License validation result {}", validationResult.isSuccessful());
                            if(validationResult.isSuccessful()== false)
                                throw new FailedOperationException("Invalid license for NetApp "+ bp.getNetAppPackageId());
                        } catch (NotExistingEntityException e) {
                            throw new FailedOperationException(e.getMessage());
                        } catch (it.nextworks.catalogue.exceptions.FailedOperationException e) {
                            throw new FailedOperationException(e.getMessage());
                        }
                    }else{
                        log.debug("Skipping open license");
                    }
                }
            }else{
                log.debug("No Licenses associated to NetApp");

            }
        }
        return true;
    }



}
