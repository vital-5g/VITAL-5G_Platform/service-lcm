package it.nextworks.lcm.vs.repo;

import it.nextworks.lcm.vs.elements.ApplicationMetricRecord;
import it.nextworks.lcm.vs.elements.InfrastructureMetricRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApplicationMetricRepo extends JpaRepository<ApplicationMetricRecord, Long> {
}
