package it.nextworks.lcm.vs.repo;

import it.nextworks.lcm.vs.elements.InfrastructureMetricRecord;
import it.nextworks.lcm.vs.elements.VerticalServiceInstanceInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface InfrastructureMetricRepo extends JpaRepository<InfrastructureMetricRecord, Long> {
}
