/*
 * Copyright (c) 2019 Nextworks s.r.l
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.nextworks.lcm.vs.engine.messages;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import it.nextworks.lcm.vs.messages.ScaleVsRequest;

import java.util.UUID;

public class ScaleVsiRequestMessage extends VsmfEngineMessage {

	@JsonProperty("vsiId")
	private UUID vsiId;
	@JsonProperty("request")
	private ScaleVsRequest request;

	@JsonCreator
	public ScaleVsiRequestMessage(@JsonProperty("vsiId") UUID vsiId, @JsonProperty("request")  ScaleVsRequest request) {
		this.type = VsmfEngineMessageType.SCALE_VSI_REQUEST;
		this.vsiId = vsiId;
		this.request=request;
	}

	/**
	 * @return the vsiId
	 */
	public UUID getVsiId() {
		return vsiId;
	}


	public ScaleVsRequest getRequest() {
		return request;
	}
}
