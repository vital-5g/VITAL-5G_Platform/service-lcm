package it.nextworks.lcm.vs.control;

import it.nextworks.catalogue.elements.InfrastructureMetric;
import it.nextworks.catalogue.elements.RuntimeAction;
import it.nextworks.lcm.vs.messages.ScaleVsRequest;

import java.util.UUID;

public class RuntimeActionRecord {

    private UUID vsiId;
    private UUID runtimeId;
    private InfrastructureMetric infrastructureMetric;
    private RuntimeAction runtimeAction;
    private String metricTopic;
    private boolean active=false;



    public RuntimeActionRecord(UUID vsiId, UUID runtimeId, RuntimeAction runtimeAction, String metricTopic) {
        this.vsiId = vsiId;
        this.runtimeId = runtimeId;
        //this.infrastructureMetric = infrastructureMetric;
        this.runtimeAction = runtimeAction;
        this.metricTopic = metricTopic;

    }



    public UUID getRuntimeId() {
        return runtimeId;
    }

    public InfrastructureMetric getInfrastructureMetric() {
        return infrastructureMetric;
    }

    public RuntimeAction getRuntimeAction() {
        return runtimeAction;
    }

    public String getMetricTopic() {
        return metricTopic;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
