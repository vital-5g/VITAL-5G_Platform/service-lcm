package it.nextworks.lcm.vs.sbi.vim;

import it.nextworks.inventory.elements.TestbedNfvoInformation;
import it.nextworks.lcm.vs.elements.Testbed;
import it.nextworks.lcm.vs.exceptions.FailedOperationException;
import it.nextworks.lcm.vs.sbi.inventory.MultiSiteInventoryService;
import org.openstack4j.api.Builders;
import org.openstack4j.api.OSClient;
import org.openstack4j.model.common.Identifier;
import org.openstack4j.model.compute.Flavor;
import org.openstack4j.model.compute.Server;
import org.openstack4j.openstack.OSFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class VimLcmService {

    private static final Logger log = LoggerFactory.getLogger(VimLcmService.class);
    @Autowired
    private MultiSiteInventoryService multiSiteInventoryService;

    @Value("${environment:development}")
    private String environment;


    public void scaleVM(Testbed testbed, String vmId, int raCpus, int raRam) throws FailedOperationException {
        log.debug("Received request to scale VM with ID:{} in testbed {}",vmId, testbed);

        OSClient.OSClientV3 os  =  getTestbedDriver(testbed);
        Server server= os.compute().servers().get(vmId);

        int cpus= server.getFlavor().getVcpus();
        if(raCpus>0){
            cpus=raCpus;
        }
        int cpusF = cpus;
        int ram= server.getFlavor().getRam();
        if(raRam>0){
            ram=raRam;
        }



        int ramF = ram;
        int disk = server.getFlavor().getDisk();
        List<? extends Flavor> flavors = os.compute().flavors().list();
        Optional<? extends Flavor> nFlavorO= flavors.stream().filter(f -> f.getVcpus()==cpusF && f.getRam()==ramF && f.getDisk()==disk).findFirst();

        if(nFlavorO.isPresent()){
            log.debug("Found matching flavor. Applying resize");
            os.compute().servers().resize(server.getId(), nFlavorO.get().getId());
            os.compute().servers().confirmResize(server.getId());
        }else {
            log.debug("Flavor not found. Creating one");
            Flavor flavor = Builders.flavor()
                    .name("V5G_"+cpus+"_"+ram+"_"+disk)
                    .ram(ram)
                    .vcpus(cpus)
                    .disk(disk)
                    .build();
            flavor = os.compute().flavors().create(flavor);
            os.compute().servers().resize(server.getId(), flavor.getId());
            os.compute().servers().confirmResize(server.getId());

        }

    }

    private OSClient.OSClientV3 getTestbedDriver(Testbed testbed) throws FailedOperationException {
        TestbedNfvoInformation nfvoInformation = multiSiteInventoryService.getTestbedNfvoInformation(testbed,
                environment);

        log.debug("Retrieved VIM info: {}", nfvoInformation.getVimEndpoint(), nfvoInformation.getVimUser());
        OSClient.OSClientV3 os = OSFactory.builderV3()
                .endpoint(nfvoInformation.getVimEndpoint())
                .credentials(nfvoInformation.getVimUser(), nfvoInformation.getVimSecret(), Identifier.byName(nfvoInformation.getVimDomain()))
                //.scopeToProject(Identifier.byName(nfvoInformation.getProject()))
                .authenticate();
        return  os;
    }

}
