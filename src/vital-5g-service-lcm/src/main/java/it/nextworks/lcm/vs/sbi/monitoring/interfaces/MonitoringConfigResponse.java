package it.nextworks.lcm.vs.sbi.monitoring.interfaces;

import java.util.HashMap;
import java.util.Map;

public class MonitoringConfigResponse {

    private Map<String, String> metricTopics = new HashMap<>();
    private String monitoringDashboardUrl;

    public MonitoringConfigResponse(Map<String, String> metricTopics, String monitoringDashboardUrl) {
        this.metricTopics = metricTopics;
        this.monitoringDashboardUrl = monitoringDashboardUrl;
    }

    public Map<String, String> getMetricTopics() {
        return metricTopics;
    }

    public String getMonitoringDashboardUrl() {
        return monitoringDashboardUrl;
    }
}
