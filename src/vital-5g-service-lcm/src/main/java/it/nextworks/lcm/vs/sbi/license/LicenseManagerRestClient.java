package it.nextworks.lcm.vs.sbi.license;

import it.nextworks.catalogue.elements.SoftwareLicense;
import it.nextworks.catalogue.exceptions.FailedOperationException;
import it.nextworks.catalogue.exceptions.MalformattedElementException;
import it.nextworks.catalogue.exceptions.NotExistingEntityException;
import it.nextworks.catalogue.exceptions.UnAuthorizedRequestException;
import it.nextworks.lcm.license.interfaces.LicenseValidationInterface;
import it.nextworks.lcm.license.messages.LicenseUsageUpdate;
import it.nextworks.lcm.license.messages.LicenseValidationRequest;
import it.nextworks.lcm.license.messages.LicenseValidationResult;
import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class LicenseManagerRestClient implements LicenseValidationInterface {

    private static final Logger log = LoggerFactory.getLogger(LicenseManagerRestClient.class);


    private KeycloakRestTemplate restTemplate;
    private RestTemplate noAuthRestTemplate;

    private String baseUrl;

    public LicenseManagerRestClient(KeycloakRestTemplate restTemplate, String baseUrl) {
        this.restTemplate = restTemplate;
        this.baseUrl = baseUrl;
        this.noAuthRestTemplate= new RestTemplate(new BufferingClientHttpRequestFactory(
                new SimpleClientHttpRequestFactory()
        ));
    }

    @Override
    public LicenseValidationResult validateLicense(UUID netAppId, LicenseValidationRequest licenseValidationRequest) throws NotExistingEntityException, FailedOperationException {
        log.debug("Building HTTP request for validate NetApp License");
        HttpHeaders header = new HttpHeaders();
        header.add("Content-Type", "application/json");

        HttpEntity<?> getEntity = new HttpEntity<LicenseValidationRequest>(licenseValidationRequest, header);

        String url = baseUrl+"/validate/"+netAppId;

        log.debug("Sending HTTP request to Onboard License.");
        try {
            ResponseEntity<LicenseValidationResult> httpResponse =
                    restTemplate.exchange(url, HttpMethod.POST, getEntity, LicenseValidationResult.class);


            log.debug("Response code: " + httpResponse.getStatusCode().toString());
            HttpStatus code = httpResponse.getStatusCode();

            if (code.equals(HttpStatus.CREATED)) {
                log.debug("License correctly onboarded");
                return httpResponse.getBody();

            } else if (code.equals(HttpStatus.FORBIDDEN)) {
                throw new FailedOperationException(httpResponse.getBody().toString());
            } else if (code.equals(HttpStatus.BAD_REQUEST)) {
                throw new FailedOperationException("Error during License Creation: " + httpResponse.getBody());

            } else {
                throw new FailedOperationException("Generic error during interaction with License Management module");
            }
        }catch (HttpClientErrorException e){
            if(e.getStatusCode().equals(HttpStatus.BAD_REQUEST)){
                throw new FailedOperationException(e.getMessage());
            }else throw new FailedOperationException(e.getMessage());
        }

    }

    @Override
    public void updateLicenseUsage(UUID licenseId, LicenseUsageUpdate licenseUsageUpdate) throws NotExistingEntityException, FailedOperationException, MalformattedElementException {
        log.debug("Building HTTP request for Update NetApp License Usage");
        HttpHeaders header = new HttpHeaders();
        header.add("Content-Type", "application/json");

        HttpEntity<?> getEntity = new HttpEntity<LicenseUsageUpdate>(licenseUsageUpdate, header);

        String url = baseUrl+"/use/"+licenseId;

        log.debug("Sending HTTP request to Update License Usage.");
        try {
            ResponseEntity<String> httpResponse =
                    noAuthRestTemplate.exchange(url, HttpMethod.POST, getEntity, String.class);


            log.debug("Response code: " + httpResponse.getStatusCode().toString());
            HttpStatus code = httpResponse.getStatusCode();

            if (code.equals(HttpStatus.CREATED)) {
                log.debug("License correctly updated");


            } else if (code.equals(HttpStatus.FORBIDDEN)) {
                throw new FailedOperationException(httpResponse.getBody().toString());
            } else if (code.equals(HttpStatus.BAD_REQUEST)) {
                throw new FailedOperationException("Error during License Creation: " + httpResponse.getBody());

            } else {
                throw new FailedOperationException("Generic error during interaction with License Management module");
            }
        }catch (HttpClientErrorException e){
            if(e.getStatusCode().equals(HttpStatus.BAD_REQUEST)){
                throw new FailedOperationException(e.getMessage());
            }else throw new FailedOperationException(e.getMessage());
        }
    }
}
