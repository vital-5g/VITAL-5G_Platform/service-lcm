/*
 * Copyright (c) 2019 Nextworks s.r.l
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.nextworks.lcm.vs;

import java.util.*;
import java.util.stream.Collectors;


import it.nextworks.catalogue.elements.*;
import it.nextworks.catalogue.elements.AccessLevel;
import it.nextworks.catalogue.elements.translator.NfvNsInstantiationInfo;
import it.nextworks.lcm.vs.control.RuntimeActionService;
import it.nextworks.lcm.vs.elements.*;
import it.nextworks.lcm.vs.elements.Testbed;
import it.nextworks.lcm.vs.engine.messages.*;
import it.nextworks.lcm.vs.exceptions.UnAuthorizedRequestException;
import it.nextworks.catalogue.interfaces.NetAppPackageManagementInterface;
import it.nextworks.lcm.vs.exceptions.FailedOperationException;
import it.nextworks.lcm.vs.exceptions.MalformattedElementException;
import it.nextworks.lcm.vs.exceptions.NotExistingEntityException;
import it.nextworks.lcm.vs.interfaces.VsLcmProviderInterface;
import it.nextworks.lcm.vs.interfaces.nfvo.NsLcmProviderInterface;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.NetworkServiceStatusChange;
import it.nextworks.lcm.vs.manager.VsLcmManager;
import it.nextworks.lcm.vs.messages.InstantiateVsRequest;
import it.nextworks.lcm.vs.messages.OnboardRuntimeActionRequest;
import it.nextworks.lcm.vs.messages.ScaleVsRequest;
import it.nextworks.lcm.vs.repo.ApplicationMetricRepo;
import it.nextworks.lcm.vs.repo.InfrastructureMetricRepo;
import it.nextworks.lcm.vs.repo.VerticalServiceInstanceInfoRepo;
import it.nextworks.lcm.vs.repo.VerticalServiceInstanceRepo;
import it.nextworks.lcm.vs.sbi.SecurityService;
import it.nextworks.lcm.vs.sbi.license.LicenseManagerService;
import it.nextworks.lcm.vs.sbi.monitoring.MonitoringService;
import it.nextworks.lcm.vs.sbi.nfvo.interfaces.NfvoLcmNotificationConsumerInterface;

import it.nextworks.catalogue.interfaces.TranslatorInterface;
import it.nextworks.catalogue.interfaces.VsBlueprintCatalogueInterface;
import it.nextworks.catalogue.interfaces.VsDescriptorCatalogueInterface;
import it.nextworks.lcm.vs.sbi.slicing.MultiSite5GSliceInventoryService;
import it.nextworks.lcm.vs.sbi.vim.VimLcmService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.annotation.PostConstruct;

/**
 * Front-end service for managing the incoming requests 
 * about creation, termination, etc. of Vertical Service instances.
 * 
 * It is invoked by the related REST controller and dispatches requests
 * to the centralized engine. 
 * origin/feat_gst
 * @author nextworks
 *
 */
@Service
public class VsLcmService implements VsLcmProviderInterface, NfvoLcmNotificationConsumerInterface {

	private static final Logger log = LoggerFactory.getLogger(VsLcmService.class);
	
	@Autowired
	private VsDescriptorCatalogueInterface vsDescriptorCatalogueService;
	
	@Autowired
	private VsBlueprintCatalogueInterface vsBlueprintCatalogueService;

    @Autowired
    private NetAppPackageManagementInterface netAppPackageService;
	
	@Autowired
	private VerticalServiceInstanceInfoRepo verticalServiceInstanceInfoRepo;

    @Autowired
    private InfrastructureMetricRepo infrastructureMetricRepo;


    @Autowired
    private ApplicationMetricRepo applicationMetricRepo;

    @Autowired
    private TranslatorInterface translatorService;

    @Autowired
    private VerticalServiceInstanceRepo verticalServiceInstanceRepo;


    @Autowired
    private MultiSite5GSliceInventoryService sliceInventoryService;

	@Value("${spring.rabbitmq.host}")
    private String rabbitHost;

	@Value("${spring.rabbitmq.timout:10}")
	private int rabbitTimeout;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private MonitoringService monitoringService;

    @Autowired
    private RuntimeActionService runtimeActionService;
    @Autowired
    private VimLcmService vimLcmService;

    @Autowired
    private LicenseManagerService licenseManagerService;

    @Autowired
    private SecurityService securityService;
    @Autowired
    @Qualifier(ConfigurationParameters.vsLcmQueueExchange)
    TopicExchange messageExchange;
    

    //internal map of VS LCM Managers
    //each VS LCM Manager is created when a new VSI ID is created and removed when the VSI ID is removed
    private Map<UUID, VsLcmManager> vsLcmManagers = new HashMap<>();




    @Autowired
    private NsLcmProviderInterface nsLcmProviderInterface;

    @PostConstruct
    public void configureVsLcm(){
        log.debug("configuring Vs LCM service");
        runtimeActionService.setVsLcmService(this);
    }

	@Override
	public UUID instantiateVs(InstantiateVsRequest request) throws
            NotExistingEntityException, FailedOperationException, MalformattedElementException, UnAuthorizedRequestException {
		log.debug("Received request to instantiate a new Vertical Service instance.");
		request.isValid();
		
		String tenantId = request.getTenantId();
		UUID vsdId = request.getVsdId();

        log.debug("Retrieving catalogue blueprints and descriptors");
        VsDescriptor vsd = null;
        try {
            vsd = vsDescriptorCatalogueService.getVsd(vsdId);
        } catch (it.nextworks.catalogue.exceptions.NotExistingEntityException e) {
            throw new NotExistingEntityException("Failed to retrieve VSD with ID:"+vsdId);
        } catch (it.nextworks.catalogue.exceptions.FailedOperationException e) {
            log.error("Error retrieving portal descriptors",e);
            throw new FailedOperationException(e);
        } catch (it.nextworks.catalogue.exceptions.MalformattedElementException e) {
            log.error("Error retrieving portal descriptors",e);
            throw new FailedOperationException(e);
        }catch (it.nextworks.catalogue.exceptions.UnAuthorizedRequestException e){
            log.error("Error retrieving portal descriptors",e);
            throw new UnAuthorizedRequestException(e);
        }
        VerticalServiceBlueprint vsb = null;
        try {
            vsb = vsBlueprintCatalogueService.getVerticalServiceBlueprint(vsd.getVsBlueprintId());
        } catch (it.nextworks.catalogue.exceptions.NotExistingEntityException e) {
            throw new NotExistingEntityException("Failed to retrieve VSB with ID:"+vsdId);
        } catch (it.nextworks.catalogue.exceptions.FailedOperationException e) {
            throw new FailedOperationException(e.getMessage());
        } catch (it.nextworks.catalogue.exceptions.MalformattedElementException e) {
            throw new MalformattedElementException(e.getMessage());
        }catch (it.nextworks.catalogue.exceptions.UnAuthorizedRequestException e) {
            throw new UnAuthorizedRequestException(e.getMessage());
        }


        List<NetAppBlueprint> netAppBlueprints = new ArrayList<>();
        for(ServiceComponent sc: vsb.getAtomicComponents()){
            try {
                log.debug("Retrieving NetApp with ID:"+sc.getNetAppBlueprintId());
                NetAppBlueprint nb = netAppPackageService.getNetAppBlueprint(sc.getNetAppBlueprintId());
                netAppBlueprints.add(nb);
            } catch (it.nextworks.catalogue.exceptions.UnAuthorizedRequestException e) {
                log.error("Error retrieving NetApp Package:" ,e);
                throw new UnAuthorizedRequestException(e.getMessage());
            } catch (it.nextworks.catalogue.exceptions.NotExistingEntityException e) {
                log.error("Error retrieving NetApp Package:" ,e);
                throw new NotExistingEntityException(e.getMessage());
            } catch (it.nextworks.catalogue.exceptions.FailedOperationException e) {
                log.error("Error retrieving NetApp Package:" ,e);
                throw new FailedOperationException(e.getMessage());
            }
        }


        log.debug("Blueprints and descriptors successfully retrieved");
        log.debug("Performing VSD translation");
        NfvNsInstantiationInfo nfvNsInstantiationInfo = null;
        try {
             nfvNsInstantiationInfo = translatorService.translateVsd(vsd.getVsDescriptorId());
        } catch (it.nextworks.catalogue.exceptions.NotExistingEntityException e) {
            log.error("Error retrieving VSD translation:" ,e);
            throw new NotExistingEntityException(e.getMessage());
        } catch (it.nextworks.catalogue.exceptions.FailedOperationException e) {
            log.error("Error retrieving NetApp Package:" ,e);
            throw new FailedOperationException(e.getMessage());
        }
        //checking configuration parameters
        Map<String, String> configurationParameters = request.getConfigurationParameters();
        List<String> providedConfigParameters = configurationParameters.keySet().stream().collect(Collectors.toList());
        List<String> acceptableConfigParameters = vsb.getConfigurableParameters().keySet().stream().collect(Collectors.toList());
        if(!acceptableConfigParameters.containsAll(providedConfigParameters)){
            String msg = "The request includes a configuration parameter which is not present in the VSB. Not acceptable. Provided:"
                    +providedConfigParameters+" Acceptable:"+acceptableConfigParameters;
            log.error(msg);
            throw new MalformattedElementException(msg);
        }
        log.debug("The VS instantiation request is valid.");
        VerticalServiceInstanceInfo vsInfo =
                new VerticalServiceInstanceInfo(request.getName(), Testbed.valueOf(vsb.getTestbed().toString()), request.getAccessLevel(), securityService.getUsername() );
        verticalServiceInstanceInfoRepo.saveAndFlush(vsInfo);
        log.debug("Created VSI info record");


        VerticalServiceInstance vsi = new VerticalServiceInstance(request.getName(), request.getDescription(),
                request.getVsdId(), securityService.getUsername(), request.getConfigurationParameters(), request.getServiceParameters(),
                request.getImsiIds(), vsInfo, Testbed.valueOf(vsb.getTestbed().toString()));

        verticalServiceInstanceRepo.saveAndFlush(vsi);
        log.debug("Created VSI record");
        UUID vsiId = vsInfo.getVerticalServiceInstanceId();
        if(vsb.getRequiredLicenses()!= null && !vsb.getRequiredLicenses().isEmpty()){
            log.debug("Validating provided licenses");
            for(String reqLic: vsb.getRequiredLicenses()){
                if(!request.getLicenseKeys().containsKey(reqLic)){
                    throw new MalformattedElementException("Missing required license key: "+reqLic);
                }
            }
        }
        boolean licenseValidation =  licenseManagerService.validateNetworkApplicationLicenses(netAppBlueprints, request.getLicenseKeys(), vsiId);
        initNewVsLcmManager(vsInfo.getVerticalServiceInstanceId(), vsd, vsb, netAppBlueprints, nfvNsInstantiationInfo,request.getLicenseKeys());

        try {
            String topic = "lifecycle.instantiatevs." + vsInfo.getVerticalServiceInstanceId();
            InstantiateVsiRequestMessage internalMessage = new InstantiateVsiRequestMessage(vsInfo.getVerticalServiceInstanceId(), request);
            try {
                sendMessageToQueue(internalMessage, topic);
            } catch (JsonProcessingException e) {
                String msg = "Error while translating internal VS instantiation message in Json format.";
                log.error(msg);
                failVsInstance(vsiId, msg);
            }


        } catch (Exception e) {
            log.error("Error:",e);
            failVsInstance(vsiId, e.getMessage());
            throw new FailedOperationException(e.getMessage());
        }
        return vsiId;

	}



	@Override
	public List<VerticalServiceInstanceInfo> queryVs(String tenant, Testbed testbed, String usecase, UUID vsbId, UUID vsdId)
			throws it.nextworks.lcm.vs.exceptions.NotExistingEntityException, it.nextworks.lcm.vs.exceptions.FailedOperationException, it.nextworks.lcm.vs.exceptions.MalformattedElementException{
		log.debug("Received query vertical service instances:", tenant, testbed, usecase, vsbId, vsdId);
		//TODO implement filters
        List<VerticalServiceInstanceInfo> result = verticalServiceInstanceInfoRepo.findAll();

        return result.stream().filter(info -> authorizeVsiAccess(info.getVerticalServiceInstanceId()))
                .collect(Collectors.toList());
	}



	@Override
	public VerticalServiceInstance getVerticalServiceInstance(UUID verticalServiceInstanceId)
            throws NotExistingEntityException, FailedOperationException, MalformattedElementException, UnAuthorizedRequestException {
		log.debug("Received request about all the IDs of Vertical Service instances.");
		if(verticalServiceInstanceId ==null)
            throw new MalformattedElementException("Missing Vertical Service Instance Id");
        if(authorizeVsiAccess(verticalServiceInstanceId)) {
            Optional<VerticalServiceInstance> vsiO = verticalServiceInstanceRepo.findByVerticalServiceInstanceId(verticalServiceInstanceId);
            if (!vsiO.isPresent()) {
                throw new NotExistingEntityException("Could not retrieve vertical service instance with id:" + verticalServiceInstanceId);
            }
            return vsiO.get();
        }else throw new UnAuthorizedRequestException("Unauthorized access to vertical service instance:"+verticalServiceInstanceId);
	}

	@Override
	public void terminateVs(UUID verticalServiceInstanceId) throws
            NotExistingEntityException, FailedOperationException, MalformattedElementException, UnAuthorizedRequestException {
		log.debug("Received request to terminate a Vertical Service instance.", verticalServiceInstanceId);
        if(!authorizeVsiAccess(verticalServiceInstanceId))
            throw new UnAuthorizedRequestException("Unauthorized access to vertical service instance:"+verticalServiceInstanceId);
        if (vsLcmManagers.containsKey(verticalServiceInstanceId)) {
            String topic = "lifecycle.terminatevs." + verticalServiceInstanceId;
            TerminateVsiRequestMessage internalMessage = new TerminateVsiRequestMessage(verticalServiceInstanceId);
            try {
                sendMessageToQueue(internalMessage, topic);
            } catch (JsonProcessingException e) {
                String msg = "Error while translating internal VS termination message in Json format.";
                failVsInstance(verticalServiceInstanceId, msg);

            }
        } else {
            log.error("Unable to find VS LCM Manager for VSI ID " + verticalServiceInstanceId + ". Unable to terminate the VSI.");
            throw new NotExistingEntityException("Unable to find VS LCM Manager for VSI ID " + verticalServiceInstanceId + ". Unable to terminate the VSI.");
        }

	}

    @Override
    public void deleteVs(UUID vsiId, boolean force) throws NotExistingEntityException, FailedOperationException, MalformattedElementException {
        log.debug("Received request to delete VS");
        Optional<VerticalServiceInstanceInfo> vsiInfo = verticalServiceInstanceInfoRepo.findById(vsiId);
        if(vsiId==null){
            throw  new MalformattedElementException("The VS Instance ID is null");
        }
        if(!vsiInfo.get().getActiveExperiments().isEmpty() && !force){
            throw new  FailedOperationException("There are some Experiments associated to the VS Instance. Try to force delete");
        }
        this.vsLcmManagers.remove(vsiId);
        verticalServiceInstanceInfoRepo.deleteById(vsiId);
    }

    @Override
    public void useVs(UUID vsiId, UUID experimentId) throws NotExistingEntityException, FailedOperationException, MalformattedElementException {
        log.debug("Received request to use VS");
        Optional<VerticalServiceInstanceInfo> vsiInfo = verticalServiceInstanceInfoRepo.findById(vsiId);
        if(vsiId==null){
            throw  new MalformattedElementException("The VS Instance ID is null");
        }
        if(experimentId==null){
            throw  new MalformattedElementException("The Experiment ID is null");
        }
        if(!vsiInfo.isPresent())
            throw new NotExistingEntityException("Vertical Service Instance with ID:"+vsiId+" not found");

        vsiInfo.get().addActiveExperiment(experimentId);
        verticalServiceInstanceInfoRepo.saveAndFlush(vsiInfo.get());

    }

    @Override
    public void releaseVs(UUID vsiId, UUID experimentId) throws NotExistingEntityException, FailedOperationException, MalformattedElementException {
        log.debug("Received request to release VS");
        Optional<VerticalServiceInstanceInfo> vsiInfo = verticalServiceInstanceInfoRepo.findById(vsiId);
        if(vsiId==null){
            throw  new MalformattedElementException("The VS Instance ID is null");
        }
        if(experimentId==null){
            throw  new MalformattedElementException("The Experiment ID is null");
        }
        if(!vsiInfo.isPresent())
            throw new NotExistingEntityException("Vertical Service Instance with ID:"+vsiId+" not found");

        vsiInfo.get().removeActiveExperiment(experimentId);
        verticalServiceInstanceInfoRepo.saveAndFlush(vsiInfo.get());
    }

    @Override
    public void scaleVs(ScaleVsRequest request) throws NotExistingEntityException, FailedOperationException, MalformattedElementException, UnAuthorizedRequestException {

        UUID vsiId = request.getVerticalServiceInstanceId();
        log.debug("Received request to scale a Vertical Service instance.", vsiId);
        if(!authorizeVsiAccess(vsiId))
            throw new UnAuthorizedRequestException("Unauthorized access to vertical service instance:"+vsiId);
        if (vsLcmManagers.containsKey(vsiId)) {
            String topic = "lifecycle.scalevs." + vsiId;
            ScaleVsiRequestMessage internalMessage = new ScaleVsiRequestMessage(vsiId, request );
            try {
                sendMessageToQueue(internalMessage, topic);
            } catch (JsonProcessingException e) {
                String msg = "Error while sending internal VS scale message in Json format.";
                failVsInstance(vsiId, msg);

            }
        } else {
            log.error("Unable to find VS LCM Manager for VSI ID " + vsiId + ". Unable to terminate the VSI.");
            throw new NotExistingEntityException("Unable to find VS LCM Manager for VSI ID " + vsiId + ". Unable to terminate the VSI.");
        }
    }

    public UUID onboardRuntimeAction(UUID vsiId, OnboardRuntimeActionRequest request) throws UnAuthorizedRequestException, NotExistingEntityException {

        log.debug("Received request to Onboardg a runtimeaction in Vertical Service instance.", vsiId);
        if(!authorizeVsiAccess(vsiId))
            throw new UnAuthorizedRequestException("Unauthorized access to vertical service instance:"+vsiId);
        if (vsLcmManagers.containsKey(vsiId)) {
            return runtimeActionService.onboardRuntimeAction(vsiId, vsLcmManagers.get(vsiId), request );
        }else{
            log.error("Unable to find VS LCM Manager for VSI ID " + vsiId + ". Unable to add runtime to the VSI.");
            throw new NotExistingEntityException("Unable to find VS LCM Manager for VSI ID " + vsiId + ". Unable to add rutnime the VSI.");
        }
    }


    private void failVsInstance(UUID vsInstanceId, String errorMsg){
        log.debug("Updating VSI status to failed");
        Optional<VerticalServiceInstance> vsi = verticalServiceInstanceRepo.findByVerticalServiceInstanceId(vsInstanceId);
        if(vsi.isPresent()){
            VerticalServiceInstance vsiR = vsi.get();
            vsiR.setStatus(VerticalServiceInstanceStatus.FAILED);
            vsiR.setErrorMsg(errorMsg);
            verticalServiceInstanceRepo.saveAndFlush(vsiR);
        }else log.error("Could not retrieve instance record. IGNORING");

    }

    /**
     * This methd is invoked by VsLcmManager as soon as its own termination process succeeded
     *
     * @param vsiId Id of terminated VSI
     * @throws NotExistingEntityException
     */
    public void notifyVsiTermination(String vsiId) throws NotExistingEntityException {
        log.debug("Processing VSI termination request for VSI ID " + vsiId);
        if (vsLcmManagers.containsKey(vsiId)) {
            log.debug("VSI " + vsiId + " TERMINATED. Removing manager");
            vsLcmManagers.remove(vsiId);

        }else{
                log.error("Unable to find VS LCM Manager for VSI ID " + vsiId + ". Invalid Termination notification.");
                throw new NotExistingEntityException("Unable to find VS LCM Manager for VSI ID " + vsiId + ". Invalid Termination notification.");


        }
    }

    /**
     * This method removes a VS LC manager from the engine map
     *
     * @param vsiId ID of the VS whose VS LCM must be removed
     */
    public void removeVerticalServiceLcmManager(String vsiId) {
        log.debug("Vertical service " + vsiId + " has been terminated. Removing VS LCM from engine");
        this.vsLcmManagers.remove(vsiId);
        log.debug("VS LCM manager removed from engine.");
    }

	@Override
	public void notifyNfvNsStatusChange(String nfvNsId, NetworkServiceStatusChange changeType, boolean b, Testbed testbed) {
		String networkServiceInstanceId = nfvNsId;

        log.debug("Processing notification about status change for network slice " + networkServiceInstanceId);
        //TODO: why is the domain not included in the notification?

		Optional<VerticalServiceInstance> vsi =
                verticalServiceInstanceRepo.findByNetworkServiceInstanceIdAndTestbed(networkServiceInstanceId, testbed);
        UUID vsiId = vsi.get().getVerticalServiceInstanceId();
        log.debug("Network service instance " + networkServiceInstanceId + " is associated to vertical service " + vsiId);
        if (vsLcmManagers.containsKey(vsiId)) {
            String topic = "lifecycle.notifyns." + vsiId;
            NotifyNsiStatusChange internalMessage = new NotifyNsiStatusChange(networkServiceInstanceId, changeType);
            try {
                sendMessageToQueue(internalMessage, topic);
            } catch (Exception e) {
                log.error("General exception while sending message to queue.");
            }
        } else {
            log.error("Unable to find Vertical Service LCM Manager for VSI ID " + vsiId + ". Unable to notify associated NS status change.");
        }


	}

    /**
     * This method initializes a new VS LCM manager that will be in charge
     * of processing all the requests and events for that VSI.
     *
     * @param vsiId ID of the VS instance for which the VS LCM Manager must be initialized
     */
    private void initNewVsLcmManager(UUID vsiId, VsDescriptor vsd, VerticalServiceBlueprint vsb, List<NetAppBlueprint> netAppBlueprints, NfvNsInstantiationInfo nfvNsInstantiationInfo, Map<String, String> licenseKeys) {
        log.debug("Initializing new VS LCM manager for VSI ID " + vsiId);
        VsLcmManager vsLcmManager = new VsLcmManager(vsiId,
                verticalServiceInstanceInfoRepo,
                verticalServiceInstanceRepo,
                infrastructureMetricRepo,
                applicationMetricRepo,
        		vsd,
        		vsb,
                netAppBlueprints,
                nfvNsInstantiationInfo,
        		translatorService, 
       		this,
        		nsLcmProviderInterface,
                monitoringService,
                sliceInventoryService,
                runtimeActionService,
                vimLcmService,
                licenseKeys, licenseManagerService);
        createQueue(vsiId, vsLcmManager);
        vsLcmManagers.put(vsiId, vsLcmManager);
        log.debug("VS LCM manager for VSI ID " + vsiId + " initialized and added to the engine.");
    }
    
    /**
     * This internal method sends a message to the internal queue, using a specific topic
     *
     * @param msg
     * @param topic
     * @throws JsonProcessingException
     */
    private void sendMessageToQueue(VsmfEngineMessage msg, String topic) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(msg);
        rabbitTemplate.convertAndSend(messageExchange.getName(), topic, json);
    }






    /**
     * This internal method creates a queue for the exchange of asynchronous messages
     * related to a given VSI.
     *
     * @param vsiId ID of the VSI for which the queue is created
     * @param vsiManager VSI Manager in charge of processing the queue messages
     */
    private void createQueue(UUID vsiId, VsLcmManager vsiManager) {
        String queueName = ConfigurationParameters.vsLcmQueueNamePrefix + vsiId;
        log.debug("Creating new Queue " + queueName + " in rabbit host " + rabbitHost);
        CachingConnectionFactory cf = new CachingConnectionFactory();
        cf.setAddresses(rabbitHost);
        cf.setConnectionTimeout(rabbitTimeout);
        RabbitAdmin rabbitAdmin = new RabbitAdmin(cf);
        Queue queue = new Queue(queueName, false, false, true);
        rabbitAdmin.declareQueue(queue);
        rabbitAdmin.declareExchange(messageExchange);
        rabbitAdmin.declareBinding(BindingBuilder.bind(queue).to(messageExchange).with("lifecycle.*." + vsiId));
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(cf);
        MessageListenerAdapter adapter = new MessageListenerAdapter(vsiManager, "receiveMessage");
        container.setMessageListener(adapter);
        container.setQueueNames(queueName);
        container.start();
        log.debug("Queue created");
    }

    private boolean authorizeVsiAccess(UUID vsiIdentifier){
        String username = securityService.getUsername();

        Optional<VerticalServiceInstanceInfo>  info = verticalServiceInstanceInfoRepo.findById(vsiIdentifier);
        if(securityService.userIsPlatformAdmin()){
            log.debug("User is admin. Access granted");
            return true;
        }else  if(info.get().getOwner()!=null && info.get().getOwner().equals(username)){
            log.debug("User is owner. Access granted");
            return true;
        }else if(info.get().getAccessLevel()==null || info.get().getAccessLevel().equals(AccessLevel.PUBLIC)){
            log.debug("Instance is public or not specified. Access granted");
            return true;

        }else return false;
    }

}
