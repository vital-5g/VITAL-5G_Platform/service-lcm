package it.nextworks.lcm.vs.sbi.nfvo;

import it.nextworks.inventory.elements.NfvoDriverType;
import it.nextworks.inventory.elements.TestbedNfvoInformation;
import it.nextworks.lcm.nfvo.osm.Osm10Client;
import it.nextworks.lcm.vs.elements.Testbed;
import it.nextworks.lcm.vs.exceptions.FailedOperationException;
import it.nextworks.lcm.vs.interfaces.nfvo.NsLcmProviderInterface;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.CreateNsIdentifierRequestInternal;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.InstantiateNsRequestInternal;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.OperationStatus;
import it.nextworks.lcm.vs.sbi.inventory.MultiSiteInventoryService;

import it.nextworks.lcm.vs.sbi.nfvo.dummy.DummyNfvoLcmDriver;

import it.nextworks.lcm.vs.sbi.nfvo.polling.NfvoLcmOperationPollingManager;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.NfvoLcmOperationType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class NfvoLcmService implements NsLcmProviderInterface {

    private static final Logger log = LoggerFactory.getLogger(NfvoLcmService.class);

    @Autowired
    private MultiSiteInventoryService multiSiteInventoryService;


    @Autowired
    private NfvoLcmOperationPollingManager pollingManager;

    @Value("${environment:development}")
    private String environment;

    //private Map<Testbed, NsLcmProviderInterface> drivers = new HashMap<>();

    @Override
    public String createNetworkServiceIdentifier(CreateNsIdentifierRequestInternal request) throws FailedOperationException {
        log.debug("Received Create NS Identifier request");



        //}else{
        //    testbedDriver = drivers.get(request.getTestbed());
        //}
        return getTestbedDriver(request.getTestbed()).createNetworkServiceIdentifier(request);
    }

    @Override
    public String instantiateNetworkService(InstantiateNsRequestInternal request) throws FailedOperationException {
        log.debug("Received Instantiate a NS request");
        String operationId = getTestbedDriver(request.getTestbed()).instantiateNetworkService(request);
        pollingManager.addOperation(operationId, OperationStatus.SUCCESSFULLY_DONE, request.getNsInstanceId(),
                NfvoLcmOperationType.NS_INSTANTIATION, request.getTestbed());

        return operationId;

    }

    @Override
    public OperationStatus getOperationStatus(String operationId, Testbed testbed) throws FailedOperationException {
        log.debug("Received get operation status request: "+ operationId+" "+testbed);
        return getTestbedDriver(testbed).getOperationStatus(operationId, testbed);
    }

    @Override
    public Map<String, String> getNetworkServiceInstanceEndpoints(String nsInstanceId, Testbed testbed) throws FailedOperationException {
        log.debug("Received request to retrieve instance endpoints");
        return getTestbedDriver(testbed).getNetworkServiceInstanceEndpoints(nsInstanceId, testbed);
    }

    @Override
    public String terminateNetworkService(String networkServiceInstanceId, Testbed testbed) throws FailedOperationException {
        log.debug("Received Instantiate a NS request");
        String operationId = getTestbedDriver(testbed).terminateNetworkService(networkServiceInstanceId, testbed);
        pollingManager.addOperation(operationId, OperationStatus.SUCCESSFULLY_DONE, networkServiceInstanceId,
                NfvoLcmOperationType.NS_TERMINATION, testbed);

        return operationId;
    }

    private NsLcmProviderInterface getTestbedDriver(Testbed testbed) throws FailedOperationException {
        TestbedNfvoInformation nfvoInformation = multiSiteInventoryService.getTestbedNfvoInformation(testbed,
                environment);
        NsLcmProviderInterface testbedDriver = null;
        if(nfvoInformation.getNfvoDriverType()== NfvoDriverType.OSM10){
            log.debug("Creating OSM10 driver with base URL: " +nfvoInformation.getBaseUrl());
            testbedDriver = new Osm10Client(nfvoInformation);

        }else if(nfvoInformation.getNfvoDriverType()== NfvoDriverType.DUMMY){
            log.debug("Creating DUMMY driver with base URL: " +nfvoInformation.getBaseUrl());
            testbedDriver = new DummyNfvoLcmDriver();
        }
        return testbedDriver;
    }

    @Override
    public String executeDay2Action(String nsInstanceId, String actionId,  String vnfProfileId, Map<String, String> inputParams, Testbed testbed) throws FailedOperationException {
        log.debug("Received Execute Day2 in NS request");
        String day2OperationId= getTestbedDriver(testbed).executeDay2Action(nsInstanceId, actionId, vnfProfileId, inputParams, testbed);
        pollingManager.addOperation(day2OperationId, OperationStatus.SUCCESSFULLY_DONE, nsInstanceId, NfvoLcmOperationType.DAY2, testbed);
        return day2OperationId;
    }

    @Override
    public String getNfvoVmVimReference(String nsInstanceId, String vnfReference, Testbed testbed) throws FailedOperationException {
        log.debug("Received request to retrieve VNF VM vim information");
        return getTestbedDriver(testbed).getNfvoVmVimReference(nsInstanceId, vnfReference, testbed);
    }
}
