package it.nextworks.lcm.vs.sbi.nfvo.dummy;

import it.nextworks.lcm.vs.elements.Testbed;
import it.nextworks.lcm.vs.exceptions.FailedOperationException;
import it.nextworks.lcm.vs.interfaces.nfvo.NsLcmProviderInterface;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.CreateNsIdentifierRequestInternal;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.InstantiateNsRequestInternal;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.OperationStatus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class DummyNfvoLcmDriver implements NsLcmProviderInterface {
    private static final Logger log = LoggerFactory.getLogger(DummyNfvoLcmDriver.class);

    @Override
    public String createNetworkServiceIdentifier(CreateNsIdentifierRequestInternal request) throws FailedOperationException {
        log.debug("Received create ns identifier request");
        return UUID.randomUUID().toString();
    }

    @Override
    public String instantiateNetworkService(InstantiateNsRequestInternal request) throws FailedOperationException {
        log.debug("Received  request");
        return UUID.randomUUID().toString();
    }

    @Override
    public OperationStatus getOperationStatus(String operationId, Testbed testbed) throws FailedOperationException {
        return OperationStatus.SUCCESSFULLY_DONE;
    }

    @Override
    public Map<String, String> getNetworkServiceInstanceEndpoints(String nsInstanceId, Testbed tesbed) throws FailedOperationException {
        return new HashMap<>();
    }

    @Override
    public String terminateNetworkService(String networkServiceInstanceId, Testbed testbed) throws FailedOperationException {
        return UUID.randomUUID().toString();
    }

    @Override
    public String executeDay2Action(String nsInstanceId, String actionId, String vnfProfileId, Map<String, String> inputParams, Testbed testbed) throws FailedOperationException {
        return UUID.randomUUID().toString();
    }

    @Override
    public String getNfvoVmVimReference(String nsInstanceId, String vnfReference, Testbed testbed) throws FailedOperationException {
        return UUID.randomUUID().toString();
    }
}
