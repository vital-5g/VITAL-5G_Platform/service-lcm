package it.nextworks.lcm.vs.sbi.nfvo.interfaces;

import it.nextworks.lcm.vs.interfaces.nfvo.messages.InternalNsLifecycleChangeNotification;


public interface NsLcmConsumerInterface {
    public void notifyNetworkServiceStatusChange(InternalNsLifecycleChangeNotification notification) ;
}
