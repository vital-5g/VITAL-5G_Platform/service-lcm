package it.nextworks.lcm.vs.sbi.monitoring;

import it.nextworks.catalogue.elements.ApplicationMetric;
import it.nextworks.catalogue.elements.InfrastructureMetric;
import it.nextworks.catalogue.elements.InfrastructureMetricType;
import it.nextworks.lcm.vs.elements.Testbed;
import it.nextworks.lcm.vs.elements.VerticalServiceInstance;
import it.nextworks.lcm.vs.exceptions.FailedOperationException;
import it.nextworks.lcm.vs.interfaces.nfvo.NsLcmProviderInterface;
import it.nextworks.lcm.vs.repo.VerticalServiceInstanceRepo;
import it.nextworks.lcm.vs.sbi.monitoring.interfaces.MonitoringConfigResponse;
import it.nextworks.lcm.vs.sbi.monitoring.interfaces.MonitoringProvider;
import it.nextworks.nfvmano.v5g.monitoring.ApiClient;
import it.nextworks.nfvmano.v5g.monitoring.ApiException;
import it.nextworks.nfvmano.v5g.monitoring.api.ApplicationMetricsApi;
import it.nextworks.nfvmano.v5g.monitoring.api.InfrastructureMetricsApi;
import it.nextworks.nfvmano.v5g.monitoring.model.BaseMetric;
import it.nextworks.nfvmano.v5g.monitoring.model.CreateMetricResponse;
import it.nextworks.nfvmano.v5g.monitoring.model.MonitoringInfrastructureMetric;
import it.nextworks.nfvmano.v5g.monitoring.model.ServiceMetric;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MonitoringService implements MonitoringProvider {
    private static final Logger log = LoggerFactory.getLogger(MonitoringService.class);

    @Value("${monitoring.url:http://localhost:8080/api/v1/platform}")
    private String monitoringUrl;


    @Autowired
    private VerticalServiceInstanceRepo verticalServiceInstanceRepo;

    @Autowired
    private NsLcmProviderInterface nsLcmProviderInterface;

    @Override
    public MonitoringConfigResponse configureNetworkMonitoringMetrics(UUID vsiId, List<InfrastructureMetric> infrastructureMetrics, String nsInstanceName) throws FailedOperationException {
        log.debug("Configuring Network monitoring metrics");
        Optional<VerticalServiceInstance> optVsi = verticalServiceInstanceRepo.findByVerticalServiceInstanceId(vsiId);
        if(optVsi.isPresent()){
            Map<String, String> metricTopics = new HashMap<>();
            VerticalServiceInstance vsInstance = optVsi.get();
            String infrastuctureMonitoringDashboard = null;
            if(infrastructureMetrics!=null && !infrastructureMetrics.isEmpty()){
                log.debug("Configuring Network metrics");
                InfrastructureMetricsApi client = getInfrastructureMetricsClient();
                for(InfrastructureMetric im : infrastructureMetrics){
                    log.debug("Configuring metric:"+im.getMetricId());
                    if(im.getType().equals(InfrastructureMetricType.AVAILABILITY)
                            ||im.getType().equals(InfrastructureMetricType.RELIABILITY)){
                        log.warn("SKIPPING UNSUPPORTED METRIC: "+im.getType());
                    }else{
                        MonitoringInfrastructureMetric mIm = getInfrastructureMetric(
                                im,
                                vsiId,
                                vsInstance.getFiveGNetworkSliceId(),
                                vsInstance.getUseCase(),
                                vsInstance.getTestbed(),
                                nsInstanceName
                        );
                        try {
                            CreateMetricResponse imResponse = client.createInfrastructureMetric(mIm);
                            if(imResponse!=null){
                                infrastuctureMonitoringDashboard= imResponse.getMonitoringDashboardUrl();
                                log.debug("Received metric topic: "+im.getMetricId()+ " "+imResponse.getTopic());
                                metricTopics.put(im.getMetricId(), imResponse.getTopic());
                            }else throw new FailedOperationException("Error during monitoring metric configuration. Please contact a Platform administrator");

                        } catch (ApiException e) {
                            log.error("Error configuring infrastructure metrics", e);
                            throw new FailedOperationException(e);
                        }
                    }


                }
            }
            return new MonitoringConfigResponse(metricTopics, infrastuctureMonitoringDashboard);


        }else throw new FailedOperationException("Could not find vertical service instance with ID:"+vsiId);
    }

    @Override
    public MonitoringConfigResponse configureInfrastructureMonitoringMetrics(UUID vsInstanceId, List<InfrastructureMetric> infrastructureMetrics, String nsInstanceName) throws FailedOperationException {
        log.debug("Configuring monitoring metrics");
        Optional<VerticalServiceInstance> optVsi = verticalServiceInstanceRepo.findByVerticalServiceInstanceId(vsInstanceId);
        if(optVsi.isPresent()){
            Map<String, String> metricTopics = new HashMap<>();
            VerticalServiceInstance vsInstance = optVsi.get();
            String infrastuctureMonitoringDashboard = null;
            if(infrastructureMetrics!=null && !infrastructureMetrics.isEmpty()){
                log.debug("Configuring infrastructure metrics");
                InfrastructureMetricsApi client = getInfrastructureMetricsClient();
                for(InfrastructureMetric im : infrastructureMetrics){
                    log.debug("Configuring metric:"+im.getMetricId());
                    if(im.getType().equals(InfrastructureMetricType.AVAILABILITY)
                            ||im.getType().equals(InfrastructureMetricType.RELIABILITY)){
                        log.warn("SKIPPING UNSUPPORTED METRIC: "+im.getType());
                    }else{
                        MonitoringInfrastructureMetric mIm = getInfrastructureMetric(
                                im,
                                vsInstanceId,
                                vsInstance.getFiveGNetworkSliceId(),
                                vsInstance.getUseCase(),
                                vsInstance.getTestbed(),
                                nsInstanceName
                        );
                        try {
                            CreateMetricResponse imResponse = client.createInfrastructureMetric(mIm);
                            if(imResponse!=null){
                                infrastuctureMonitoringDashboard= imResponse.getMonitoringDashboardUrl();
                                log.debug("Received metric topic: "+im.getMetricId()+ " "+imResponse.getTopic());
                                metricTopics.put(im.getMetricId(), imResponse.getTopic());
                            }else throw new FailedOperationException("Error during monitoring metric configuration. Please contact a Platform administrator");

                        } catch (ApiException e) {
                            log.error("Error configuring infrastructure metrics", e);
                            throw new FailedOperationException(e);
                        }
                    }


                }
            }
            return new MonitoringConfigResponse(metricTopics, infrastuctureMonitoringDashboard);


        }else throw new FailedOperationException("Could not find vertical service instance with ID:"+vsInstanceId);
    }



    @Override
    public MonitoringConfigResponse configureApplicationMonitoringMetrics(UUID vsInstanceId, List<ApplicationMetric> applicationMetrics) throws FailedOperationException {
        log.debug("Configuring application monitoring metrics");
        Optional<VerticalServiceInstance> optVsi = verticalServiceInstanceRepo.findByVerticalServiceInstanceId(vsInstanceId);
        if(optVsi.isPresent()){
            Map<String, String> metricTopics = new HashMap<>();
            VerticalServiceInstance vsInstance = optVsi.get();
            String serviceMonitoringDashboardUrl = null;
            if(applicationMetrics!=null && !applicationMetrics.isEmpty()){
                log.debug("Configuring application metrics");
                ApplicationMetricsApi client = getApplicationMetricsApi();
                for(ApplicationMetric am : applicationMetrics){
                    log.debug("Configuring metric:"+am.getMetricId());
                    ServiceMetric sM = getServiceMetric(
                            am,
                            vsInstanceId,
                            vsInstance.getUseCase(),
                            vsInstance.getTestbed()
                    );
                    try {
                        CreateMetricResponse smResponse = client.createApplicationMetric(sM);
                        if(smResponse!=null){
                            serviceMonitoringDashboardUrl=smResponse.getMonitoringDashboardUrl();
                            log.debug("Received metric topic: "+sM.getMetricId()+ " "+smResponse.getTopic());
                            metricTopics.put(am.getMetricId(), smResponse.getTopic());
                        }else  throw new FailedOperationException("Error during monitoring metric configuration. Please contact a Platform administrator");

                    } catch (ApiException e) {
                        log.error("Error configuring infrastructure metrics", e);
                        throw new FailedOperationException(e);
                    }

                }
            }
            return new MonitoringConfigResponse(metricTopics,serviceMonitoringDashboardUrl) ;


        }else throw new FailedOperationException("Could not find vertical service instance with ID:"+vsInstanceId);
    }

    @Override
    public void terminateInfrastructureMonitoringMetrics(List<String> topics) throws FailedOperationException {
        log.debug("Terminating infrastructure monitoring metrics");
        ApplicationMetricsApi client = getApplicationMetricsApi();
        if(topics!=null){
            for(String topic: topics) {
                try {
                    log.debug("Deleting infrastructure metric with topic: "+topic);
                    client.deleteInfrastructureMetric(topic);
                } catch (ApiException e) {
                    log.error("Error during infrastructure Monitoring termination", e);
                    throw new FailedOperationException("Error during Application Monitoring termination. Return Code:" + e.getCode());
                } catch (Exception e) {
                    log.error("Error during infrastructure Monitoring termination", e);
                    throw new FailedOperationException("Error during Application Monitoring termination.");

                }
            }
        }
        log.debug("Finished terminating infrastructure metrics");
    }

    @Override
    public void terminateApplicationMonitoringMetrics(List<String> topics) throws FailedOperationException {
        log.debug("Terminating application monitoring metrics");
        ApplicationMetricsApi client = getApplicationMetricsApi();
        if(topics!=null){
            for(String topic: topics) {
                try {
                    log.debug("Deleting application metric with topic: "+topic);
                    client.deleteApplicationMetric(topic);
                } catch (ApiException e) {
                    log.error("Error during Application Monitoring termination", e);
                    throw new FailedOperationException("Error during Application Monitoring termination. Return Code:" + e.getCode());
                } catch (Exception e) {
                    log.error("Error during Application Monitoring termination", e);
                    throw new FailedOperationException("Error during Application Monitoring termination.");

                }
            }
        }
        log.debug("Finished terminating application metrics");
    }

    private ServiceMetric getServiceMetric(ApplicationMetric am, UUID vsInstanceId, String useCase, Testbed testbed) {
      ServiceMetric body = new ServiceMetric();
      body.setMetricId(am.getMetricId());
      //TODO: Check where to get this information from (to check WP4 metric groupings)
      body.setMetricKpi(am.getMetricId());
      body.setUseCase(useCase);
      body.setVerticalServiceInstanceId(vsInstanceId.toString());
      body.setTestbed(BaseMetric.TestbedEnum.fromValue(testbed.toString()));
      body.setUnit(am.getUnit());
      body.setMetricCollectionType(BaseMetric.MetricCollectionTypeEnum.fromValue(am.getMetricCollectionType().toString()));

      return body;
    }









    /**
     USER_DATA_RATE_DOWNLINK -> USER_EXPERIENCED_DOWNLINK_DATA_RATE
     USER_DATA_RATE_UPLINK -> USER_EXPERIENCED_UPLINK_DATA_RATE
     LATENCY_USERPLANE/CONTROLPLANE/USERPLANE_RTT -> LATENCY
     RELIABILITY,
     AVAILABILITY,
     CPU_USAGE, -> CPU_LOAD
     MEMORY_USAGE -> RAM_USAGE
     DISK_USAGE
     LATENCY,
     E2E_LATENCY,
     GPU_LOAD,
     PACKET_DELIVERY_RATIO,
     PACKET_LOSS,
     JITTER,
     DROPPED_PACKETS_PER_SECOND,
     FORWARDED_PACKETS_PER_SECOND,
     DROPPED_BYTES_PER_SECOND,
     FORWARDED_BYTES_PER_SECOND,
     CPU_LOAD,
     RAM_USAGE,
     USER_EXPERIENCED_DOWNLINK_DATA_RATE,
     USER_EXPERIENCED_UPLINK_DATA_RATE;
     */

    private MonitoringInfrastructureMetric getInfrastructureMetric(InfrastructureMetric im,
                                                                   UUID vsiId,
                                                                   String sliceId,
                                                                   String useCase,
                                                                   Testbed testbed,
                                                                   String nsInstanceName) throws FailedOperationException {
        MonitoringInfrastructureMetric body;
        body = new MonitoringInfrastructureMetric();
        VerticalServiceInstance vsi = verticalServiceInstanceRepo.findByVerticalServiceInstanceId(vsiId).get();
        //TODO: Fix this. Workaround for fixed metrics in R2 demo
        MonitoringInfrastructureMetric.MetricKPIEnum kpi = null;
        if(im.getType().toString().startsWith("LATENCY_")) {
            //Fixed this in the libraries, keeped the old values for compatibility
            log.warn("DEPRECATED Metric type used: "+im.getType().toString());
            kpi = MonitoringInfrastructureMetric.MetricKPIEnum.LATENCY;

        }else if(im.getType().toString().equals("MEMORY_USAGE")) {
            log.warn("DEPRECATED Metric type used: " + im.getType().toString());
            kpi = MonitoringInfrastructureMetric.MetricKPIEnum.RAM_USAGE;
        }else if(im.getType().equals(InfrastructureMetricType.USER_DATA_RATE_DOWNLINK)){
                log.warn("DEPRECATED Metric type used: "+im.getType().toString());
                kpi = MonitoringInfrastructureMetric.MetricKPIEnum.USER_EXPERIENCED_DOWNLINK_DATA_RATE;
        }else if(im.getType().equals(InfrastructureMetricType.USER_DATA_RATE_UPLINK)){
            log.warn("DEPRECATED Metric type used: "+im.getType().toString());
            kpi = MonitoringInfrastructureMetric.MetricKPIEnum.USER_EXPERIENCED_UPLINK_DATA_RATE;
        }else if(im.getType().equals(InfrastructureMetricType.CPU_USAGE)){
            log.warn("DEPRECATED Metric type used: "+im.getType().toString());
            kpi = MonitoringInfrastructureMetric.MetricKPIEnum.CPU_LOAD;
        }else{
            kpi = MonitoringInfrastructureMetric.MetricKPIEnum.fromValue(im.getType().toString());
        }
        if(kpi==null){
            throw new FailedOperationException("Could NOT determine Monitoring infrastructure metric for: "+im.getType());
        }
        if(im.isNetworkMetric())
        {
            log.debug("Adding Slice ID to Infrastructure Metric");
            body.setSliceId(sliceId);
        }

        if(kpi.equals(MonitoringInfrastructureMetric.MetricKPIEnum.CPU_LOAD)
         || kpi.equals(MonitoringInfrastructureMetric.MetricKPIEnum.RAM_USAGE)
                || kpi.equals(MonitoringInfrastructureMetric.MetricKPIEnum.DISK_USAGE)
                || kpi.equals(MonitoringInfrastructureMetric.MetricKPIEnum.GPU_LOAD)
        ){
            log.debug("Adding vmReference to Infrastructure Metric");
            if(im.getVnfReference()==null) throw new FailedOperationException("Infrastructure metric without VNF reference."+im.getMetricId());

            String vmReference = nsLcmProviderInterface.getNfvoVmVimReference(vsi.getNetworkServiceInstanceId(), im.getVnfReference(), testbed);
            body.setVmReference(vmReference);
        }
        body.setMetricKPI(kpi);
        body.setInfrastructureMetricType(MonitoringInfrastructureMetric.InfrastructureMetricTypeEnum.fromValue(im.getType().toString()));

        body.setVerticalServiceInstanceId(vsiId.toString());
        body.setUseCase(useCase);
        body.setUnit(im.getUnit());
        body.setMetricId(im.getMetricId());
        body.setMetricCollectionType(BaseMetric.MetricCollectionTypeEnum.fromValue(im.getMetricCollectionType().toString()));
        if(testbed == Testbed.DANUBE){
            body.setTestbed(BaseMetric.TestbedEnum.GALATI);
        }else{
            body.setTestbed(BaseMetric.TestbedEnum.fromValue(testbed.toString()));
        }

        return body;
    }


    private InfrastructureMetricsApi getInfrastructureMetricsClient(){
        log.debug("Creating infrastructure metric client");
        InfrastructureMetricsApi client = new InfrastructureMetricsApi();
        ApiClient apiClient = new ApiClient();
        apiClient.setBasePath(this.monitoringUrl)
                .setConnectTimeout(0)
                .setWriteTimeout(0)
                .setDebugging(true)
                .setReadTimeout(0);
        client.setApiClient(apiClient);
        return client;
    }


    private ApplicationMetricsApi getApplicationMetricsApi(){
        log.debug("Creating application metric client");
        ApplicationMetricsApi client = new ApplicationMetricsApi();
        ApiClient apiClient = new ApiClient();
        apiClient.setBasePath(this.monitoringUrl)
                .setConnectTimeout(0)
                .setWriteTimeout(0)
                .setDebugging(true)
                .setReadTimeout(0);
        client.setApiClient(apiClient);
        return client;
    }

}
