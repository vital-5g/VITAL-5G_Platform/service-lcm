package it.nextworks.lcm.vs.manager;

public enum NetworkServiceStatus {
    INSTANTIATED,
    MODIFIED,
    TERMINATED,
    FAILED;
}
