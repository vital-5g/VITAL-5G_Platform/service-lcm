package it.nextworks.lcm.vs.repo;

import it.nextworks.lcm.vs.elements.Testbed;
import it.nextworks.lcm.vs.elements.VerticalServiceInstance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface VerticalServiceInstanceRepo extends JpaRepository<VerticalServiceInstance, Long> {
    Optional<VerticalServiceInstance> findByVerticalServiceInstanceId(UUID vsInstanceId);

    Optional<VerticalServiceInstance> findByNetworkServiceInstanceIdAndTestbed(String networkServiceInstanceId, Testbed testbed);
}
