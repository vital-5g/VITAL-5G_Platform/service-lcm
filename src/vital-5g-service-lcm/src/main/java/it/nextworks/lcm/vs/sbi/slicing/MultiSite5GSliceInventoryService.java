package it.nextworks.lcm.vs.sbi.slicing;

import it.nextworks.catalogue.elements.EMBBSliceProfile;
import it.nextworks.catalogue.elements.EMBBSliceProfileParams;

import it.nextworks.catalogue.elements.URLLCSliceProfile;
import it.nextworks.catalogue.elements.URLLCSliceProfileParams;
import it.nextworks.lcm.vs.elements.Testbed;
import it.nextworks.lcm.vs.exceptions.FailedOperationException;
import it.nextworks.lcm.vs.sbi.nfvo.NfvoLcmService;
import it.nextworks.nfvmano.v5g.slicing.client.ApiClient;
import it.nextworks.nfvmano.v5g.slicing.client.ApiException;
import it.nextworks.nfvmano.v5g.slicing.client.api.SlicingApi;
import it.nextworks.nfvmano.v5g.slicing.client.model.SelectEmbbSliceNB;
import it.nextworks.nfvmano.v5g.slicing.client.model.SelectUrllcSliceNB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class MultiSite5GSliceInventoryService {

    private static final Logger log = LoggerFactory.getLogger(MultiSite5GSliceInventoryService.class);


    @Value("${multi-site-5g-slice-inventory.enable:false}")
    private boolean enableSliceInventory;

    @Value("${multi-site-5g-slice-inventory.url}")
    private String sliceInventoryUrl;

    @Value("${multi-site-5g-slice-inventory.default-jitter:1.0}")
    private String jitterDefault;





    public String allocateEMBBSlice(EMBBSliceProfile sliceProfile, List<String> imsiIds, Testbed testbed) throws FailedOperationException {
        log.debug("Received request to allocate a EMBB slice:"+sliceProfile);
        if(!enableSliceInventory){
            log.warn("Generating RANDOM UUID");
            return UUID.randomUUID().toString();
        }

        String sliceId = null;
        SlicingApi slicingApi= getSlicingRestClient();

        SelectEmbbSliceNB embbRequest = new SelectEmbbSliceNB();
        EMBBSliceProfileParams profileParams= sliceProfile.getProfileParams();

        embbRequest.areaTrafficCapDL(Double.valueOf(profileParams.getAreaTrafficCapDL()).floatValue());
        embbRequest.setAreaTrafficCapUL(Double.valueOf(profileParams.getAreaTrafficCapUL()).floatValue());
        String site = testbed.toString();
        if(testbed.equals(Testbed.DANUBE)||testbed.equals(Testbed.GALATI))
            site="galati";
        else if(testbed.equals(Testbed.ANTWERP))
            site="antwerp";
        else if(testbed.equals(Testbed.ATHENS))
            site="athens";
        embbRequest.setTestbed(site);
        embbRequest.setExpDataRateDL(Double.valueOf(profileParams.getExpDataRateDL()).floatValue());
        embbRequest.setExpDataRateUL(Double.valueOf(profileParams.getExpDataRateUL()).floatValue());
        embbRequest.setUserDensity(Double.valueOf(profileParams.getUserDensity()).floatValue());
        embbRequest.setUserSpeed(Double.valueOf(profileParams.getUserSpeed()).floatValue());
        embbRequest.setImsi(imsiIds);
        try {
            sliceId = slicingApi.selectSliceEmbbPost(embbRequest);
            log.debug("Received 5G Slice instance ID:",sliceId);
        } catch (ApiException e) {
            log.error("Error while allocating EMBB slice:", e);
            log.error(e.getMessage());
            throw new FailedOperationException("Error during EMBB slice allocation in VITAL-5G multi-site 5G slice inventory. Please contact a Platform administrator");
        }
        return sliceId;
    }

    public String allocateURLLCSlice(URLLCSliceProfile sliceProfile, List<String> imsiIds, Testbed testbed) throws FailedOperationException {
        log.debug("Received request to allocate a URLLC slice:"+sliceProfile);
        if(!enableSliceInventory){
            log.warn("Generating RANDOM UUID");
            return UUID.randomUUID().toString();
        }
        String sliceId = null;
        SlicingApi slicingApi= getSlicingRestClient();

        SelectUrllcSliceNB request = new SelectUrllcSliceNB();
        URLLCSliceProfileParams profileParams= sliceProfile.getProfileParams();
        if(profileParams.getExpDataRateDL()!=null){
            request.setExpDataRateDL(profileParams.getExpDataRateDL().floatValue());
        }
        if(profileParams.getExpDataRateUL()!=null){
            request.setExpDataRateUL(profileParams.getExpDataRateUL().floatValue());
        }

        String site = testbed.toString();
        if(testbed.equals(Testbed.DANUBE)||testbed.equals(Testbed.GALATI))
            site="galati";
        else if(testbed.equals(Testbed.ANTWERP))
            site="antwerp";
        else if(testbed.equals(Testbed.ATHENS))
            site="athens";
        request.setTestbed(site);

        //TODO
        if(profileParams.getJitter()!=null){
            request.setJitter(profileParams.getJitter().floatValue());
        }else
            request.setJitter(Float.valueOf(jitterDefault));

        if(profileParams.getLatency()!=null){
            request.setLatency(profileParams.getLatency().floatValue());
        }


        //TODO: To be solved a the slice inventory
        if(imsiIds!=null){
            request.setUserEquipment(imsiIds);
        }


        try {
            sliceId=slicingApi.selectSliceUrllcPost(request);
            log.debug("Received 5G Slice instance ID:",sliceId);
        } catch (ApiException e) {
            log.error("Error while allocating URLLC slice:", e);
            log.error(e.getMessage());
            throw new FailedOperationException("Error during URLLC slice allocation in VITAL-5G multi-site 5G slice inventory. Please contact a Platform administrator");
        }
        return sliceId;
    }


    public void deallocateSlice(String sliceInstanceId){


    }


    private SlicingApi getSlicingRestClient(){
        log.debug("Creating Slice inventory REST client");
        SlicingApi slicingApi = new SlicingApi();
        ApiClient apiClient = new ApiClient();
        apiClient.setBasePath(sliceInventoryUrl)
                        .setDebugging(true)
                        .setReadTimeout(0)
                        .setWriteTimeout(0)
                                                .setConnectTimeout(0);
        slicingApi.setApiClient(apiClient);
        return slicingApi;
    }
}
