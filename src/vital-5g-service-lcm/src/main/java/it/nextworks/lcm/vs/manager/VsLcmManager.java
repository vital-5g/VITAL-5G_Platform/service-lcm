/*
 * Copyright (c) 2019 Nextworks s.r.l
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.nextworks.lcm.vs.manager;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.nextworks.catalogue.elements.*;
import it.nextworks.lcm.vs.VsLcmService;
import it.nextworks.lcm.vs.control.RuntimeActionService;
import it.nextworks.lcm.vs.elements.*;
import it.nextworks.lcm.vs.engine.messages.*;
import it.nextworks.lcm.vs.exceptions.FailedOperationException;
import it.nextworks.lcm.vs.interfaces.nfvo.NsLcmProviderInterface;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.CreateNsIdentifierRequestInternal;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.InstantiateNsRequestInternal;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.NetworkServiceStatusChange;
import it.nextworks.lcm.vs.messages.ScaleVsNSRequest;
import it.nextworks.lcm.vs.messages.ScaleVsRequest;
import it.nextworks.lcm.vs.repo.ApplicationMetricRepo;
import it.nextworks.lcm.vs.repo.InfrastructureMetricRepo;
import it.nextworks.lcm.vs.repo.VerticalServiceInstanceInfoRepo;
import it.nextworks.lcm.vs.repo.VerticalServiceInstanceRepo;
import it.nextworks.lcm.vs.sbi.license.LicenseManagerService;
import it.nextworks.lcm.vs.sbi.monitoring.interfaces.MonitoringConfigResponse;
import it.nextworks.lcm.vs.sbi.monitoring.interfaces.MonitoringProvider;

import it.nextworks.catalogue.elements.translator.NfvNsInstantiationInfo;
import it.nextworks.catalogue.interfaces.TranslatorInterface;
import it.nextworks.lcm.vs.sbi.slicing.MultiSite5GSliceInventoryService;
import it.nextworks.lcm.vs.sbi.vim.VimLcmService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Entity in charge of managing the lifecycle
 * of a single vertical service instance
 *
 * @author nextworks
 *
 */
public class VsLcmManager {

    private static final Logger log = LoggerFactory.getLogger(VsLcmManager.class);
    private  Map<String, String> licenseKeys;
    private UUID vsiId;

    private VerticalServiceInstanceInfoRepo verticalServiceInstanceInfoRepo;
    private VerticalServiceInstanceRepo verticalServiceInstanceRepo;

    private ApplicationMetricRepo applicationMetricRepo;
    private InfrastructureMetricRepo infrastructureMetricRepo;
    private VsLcmService vsLcmService;
    private TranslatorInterface translatorService;
    private LicenseManagerService licenseManagerService;

    private MonitoringProvider monitoringProvider;
    private VimLcmService vimLcmService;
    private RuntimeActionService runtimeActionService;

    private MultiSite5GSliceInventoryService sliceInventoryService;

    private NsLcmProviderInterface nsLcmProvider;

    private NfvNsInstantiationInfo nfvNsInstantiationInfo;

    //Descriptors
    private VsDescriptor vsDescriptor;
    private VerticalServiceBlueprint verticalServiceBlueprint;
    private List<NetAppBlueprint> netAppBlueprints = new ArrayList<>();


    //status
    private VerticalServiceInstanceStatus  status = VerticalServiceInstanceStatus.CREATED;

    /**
     * Constructor
     *
     * @param vsiId                  ID of the vertical service instance
     * @param verticalServiceInstanceInfoRepo        wrapper of VSI record
     * @param vsDescriptor  VSD of the service
     * @param verticalServiceBlueprint  VSB of the service           
     * @param translatorService      translator service
     * @param vsLcmService                 engine
     * @param nsLcmProvider

     */
    public VsLcmManager(UUID vsiId,
                        VerticalServiceInstanceInfoRepo verticalServiceInstanceInfoRepo,
                        VerticalServiceInstanceRepo verticalServiceInstanceRepo,
                        InfrastructureMetricRepo infrastructureMetricRepo,
                        ApplicationMetricRepo applicationMetricRepo,
                        VsDescriptor vsDescriptor,
                        VerticalServiceBlueprint verticalServiceBlueprint,
                        List<NetAppBlueprint> netAppBlueprints,
                        NfvNsInstantiationInfo nfvNsInstantiationInfo,
                        TranslatorInterface translatorService,
                        VsLcmService vsLcmService,
                        NsLcmProviderInterface nsLcmProvider,
                        MonitoringProvider monitoringProvider,
                        MultiSite5GSliceInventoryService sliceInventoryService,
                        RuntimeActionService runtimeActionService,
                        VimLcmService vimLcmService,
                        Map<String, String > licenseKeys,
                        LicenseManagerService licenseManagerService

    ) {
        this.vsiId = vsiId;
        this.verticalServiceInstanceInfoRepo = verticalServiceInstanceInfoRepo;
        this.verticalServiceInstanceRepo = verticalServiceInstanceRepo;
        this.vsDescriptor = vsDescriptor;
        this.verticalServiceBlueprint = verticalServiceBlueprint;
        this.translatorService = translatorService;
        this.vsLcmService = vsLcmService;
        this.nsLcmProvider = nsLcmProvider;
        this.monitoringProvider=monitoringProvider;
        this.nfvNsInstantiationInfo=nfvNsInstantiationInfo;
        if(netAppBlueprints!=null) this.netAppBlueprints = netAppBlueprints;
        this.sliceInventoryService=sliceInventoryService;
        this.infrastructureMetricRepo=infrastructureMetricRepo;
        this.applicationMetricRepo=applicationMetricRepo;
        this.runtimeActionService= runtimeActionService;
        this.vimLcmService=vimLcmService;
        this.licenseKeys= licenseKeys;
        this.licenseManagerService = licenseManagerService;
    }

    /**
     * Method used to receive messages about VSI lifecycle from the Rabbit MQ
     *
     * @param message received message
     */
    public void receiveMessage(String message) {
        log.debug("Received message for VSI " + vsiId + "\n" + message);

        try {
            ObjectMapper mapper = new ObjectMapper();
            VsmfEngineMessage em = mapper.readValue(message, VsmfEngineMessage.class);
            VsmfEngineMessageType type = em.getType();

            switch (type) {
                case INSTANTIATE_VSI_REQUEST: {
                    log.debug("Processing VSI instantiation request.");
                    InstantiateVsiRequestMessage instantiateVsRequestMsg = (InstantiateVsiRequestMessage) em;
                    processInstantiateRequest(instantiateVsRequestMsg);
                    break;
                }
                case TERMINATE_VSI_REQUEST: {
                    log.debug("Processing VSI termination request.");
                    TerminateVsiRequestMessage terminateVsRequestMsg = (TerminateVsiRequestMessage) em;
                    processTerminateRequest(terminateVsRequestMsg);
                    break;
                }
                case SCALE_VSI_REQUEST: {
                    log.debug("Processing VSI Scale request.");
                    ScaleVsiRequestMessage scaleVsRequestMsg = (ScaleVsiRequestMessage) em;
                    processScaleRequest(scaleVsRequestMsg);
                    break;
                }

                case NOTIFY_NSI_STATUS_CHANGE: {
                    log.debug("Processing NSI status change notification.");
                    NotifyNsiStatusChange notifyNsiStatusChangeMsg = (NotifyNsiStatusChange) em;
                    processNsiStatusChangeNotification(notifyNsiStatusChangeMsg);
                    break;
                }

                default:
                    log.error("Received message with not supported type. Skipping.");
                    break;
            }

        } catch (JsonParseException e) {
            manageVsError("Error while parsing message: " + e.getMessage());
        } catch (JsonMappingException e) {
            manageVsError("Error in Json mapping: " + e.getMessage());
        } catch (IOException e) {
            manageVsError("IO error when receiving json message: " + e.getMessage());
        }catch (Exception e){
            log.error("Unhandled exception");
            manageVsError("Unhandled error: " + e.getMessage());
        }
    }


    void processScaleRequest(ScaleVsiRequestMessage msg){
        log.debug("Processing scaling request");
        if (!msg.getVsiId().equals(vsiId)) {
            throw new IllegalArgumentException(String.format("Wrong VSI ID: %s", msg.getVsiId()));
        }

        VerticalServiceInstance vsi = verticalServiceInstanceRepo.findByVerticalServiceInstanceId(this.vsiId).get();

        if (vsi.getStatus() != VerticalServiceInstanceStatus.INSTANTIATED) {
            manageVsError("Received scale request in wrong status. Skipping message.");
            return;
        }
        if(msg.getRequest().getType().equals(ScaleVsRequest.ScaleVsType.NS_SCALE)){
            ScaleVsNSRequest nsScaleReq = (ScaleVsNSRequest) msg.getRequest();
            this.scaleVs(nsScaleReq.getVnfdId(), nsScaleReq.getCpus(), nsScaleReq.getRam());
        }

    }

    void processInstantiateRequest(InstantiateVsiRequestMessage msg) {
        log.debug("Processing instantiation request");
        if (!msg.getVsiId().equals(vsiId)) {
            throw new IllegalArgumentException(String.format("Wrong VSI ID: %s", msg.getVsiId()));
        }
       
        VerticalServiceInstance vsi = verticalServiceInstanceRepo.findByVerticalServiceInstanceId(this.vsiId).get();

        if (vsi.getStatus() != VerticalServiceInstanceStatus.CREATED) {
            manageVsError("Received instantiation request in wrong status. Skipping message.");
            return;
        }

        log.debug("Instantiating Vertical Service " + vsiId + " with VSD " + this.vsDescriptor.getVsDescriptorId());
        try {
            this.
            log.debug("The VSD has been translated in the required network service characteristics.");
            log.debug("Translator response: " + nfvNsInstantiationInfo);
            CreateNsIdentifierRequestInternal request;
            request = new CreateNsIdentifierRequestInternal(
                    nfvNsInstantiationInfo.getNsdId(),
                    vsi.getName(),
                    vsi.getVerticalServiceInstanceInfo().getTestbed());
            String nsiId = nsLcmProvider.createNetworkServiceIdentifier(request);
            log.debug("Created Network Service Instance ID: "+nsiId);


            this.updateVsiStatus(VerticalServiceInstanceStatus.INSTANTIATING);
            vsi = verticalServiceInstanceRepo.findByVerticalServiceInstanceId(this.vsiId).get();
            vsi.setNetworkServiceInstanceId(nsiId);
            verticalServiceInstanceRepo.saveAndFlush(vsi);
            log.debug("Network Service ID " + nsiId + " created for VSI " + vsiId);
            nsLcmProvider.instantiateNetworkService(new InstantiateNsRequestInternal( vsi.getName(),
                    nfvNsInstantiationInfo.getNsdId(),nsiId, vsi.getTestbed(),null));
        } catch (Exception e) {
            manageVsError("Error while instantiating VS " + vsiId + ": " + e.getMessage(), e);
        }
    }

    private void updateVsiStatus(VerticalServiceInstanceStatus status){
        log.debug("Updating vertical service instance status to {}", status);
        VerticalServiceInstance vsi = verticalServiceInstanceRepo.findByVerticalServiceInstanceId(this.vsiId).get();
        VerticalServiceInstanceInfo vsiInfo = verticalServiceInstanceInfoRepo.findById(this.vsiId).get();
        log.debug("Status before:{} {}", vsiInfo.getStatus(),
                vsi.getStatus());
        vsiInfo.setStatus(status);
        verticalServiceInstanceInfoRepo.saveAndFlush(vsiInfo);
        vsi.setStatus(status);
        verticalServiceInstanceRepo.saveAndFlush(vsi);
        this.status=status;
        log.debug("Status after:{} {} {} {}",vsiInfo.getStatus(), vsi.getStatus(),  verticalServiceInstanceInfoRepo.findById(this.vsiId).get().getStatus(),
                verticalServiceInstanceRepo.findByVerticalServiceInstanceId(this.vsiId).get().getStatus());
    }

    void processTerminateRequest(TerminateVsiRequestMessage msg) {
        log.debug("Processing termination request");
        if (!msg.getVsiId().equals(vsiId)) {
            throw new IllegalArgumentException(String.format("Wrong VSI ID: %s", msg.getVsiId()));
        }
        VerticalServiceInstance vsi = verticalServiceInstanceRepo.findByVerticalServiceInstanceId(this.vsiId).get();
        VerticalServiceInstanceStatus internalStatus = vsi.getStatus();
        if (internalStatus != VerticalServiceInstanceStatus.INSTANTIATED) {
            manageVsError("Received termination request in wrong status. Skipping message.");
            return;
        }

        log.debug("Terminating Vertical Service " + vsiId);
        this.updateVsiStatus(VerticalServiceInstanceStatus.TERMINATING);

        try {

            log.debug("Terminating service monitoring metrics and runtimeactions");
            if(vsi.getRuntimeActions()!=null && !vsi.getRuntimeActions().isEmpty()){
                log.debug("Terminating runtimections");
                for(UUID rid : vsi.getRuntimeActions().values()){
                    runtimeActionService.deactivateRuntimeAction(rid);
                }
            }
            List<String> applicationTopics = vsi.getApplicationMetrics().stream().map(am -> am.getTopic()).collect(Collectors.toList());
            List<String> infrastructureTopics = vsi.getInfrastructureMetrics().stream().map(im -> im.getTopic()).collect(Collectors.toList());
            monitoringProvider.terminateApplicationMonitoringMetrics(applicationTopics);
            monitoringProvider.terminateInfrastructureMonitoringMetrics(infrastructureTopics);
            log.debug("Network service " + vsi.getNetworkServiceInstanceId() + " must be terminated.");
            nsLcmProvider.terminateNetworkService(vsi.getNetworkServiceInstanceId(), vsi.getTestbed());
        } catch (Exception e) {
            manageVsError("Error while terminating VS " + vsiId + ": " + e.getMessage());
        }
    }




  
    private void nsStatusChangeOperations(NetworkServiceStatus status, String nsiId) {
        log.debug("Received NS Status change notification");
        VerticalServiceInstance vsi = verticalServiceInstanceRepo.findByVerticalServiceInstanceId(vsiId).get();
        //VerticalServiceInstanceStatus internalStatus =vsi.getStatus();

        if (status == NetworkServiceStatus.INSTANTIATED && this.status == VerticalServiceInstanceStatus.INSTANTIATING) {
            if(this.status!=VerticalServiceInstanceStatus.INSTANTIATING){
                log.debug("Ignoring VSI Status update");
                return;
            }

            log.debug("Updating Vertical service instance internal network slice subnet");

            internalRetrieveNetworkServiceEndpoints();
            internalAllocateSlice();
            internalConfigureMonitoringMetrics();
            internalUseLicenses();
            if(this.status==VerticalServiceInstanceStatus.INSTANTIATING){

                this.updateVsiStatus(VerticalServiceInstanceStatus.INSTANTIATED);


            }

                
           
        } else if (status == NetworkServiceStatus.TERMINATED && this.status == VerticalServiceInstanceStatus.TERMINATING) {


            this.updateVsiStatus(VerticalServiceInstanceStatus.TERMINATED);

            log.debug("Termination procedure completed");
            //vsLocalEngine.notifyVsiTermination(vsiId);
            verticalServiceInstanceRepo.saveAndFlush(vsi);

        } else {
            manageVsError("Received notification about NSI creation in wrong status.");
            return;
        }

    }

    private void internalUseLicenses(){
        log.debug("Updating license usage");
        //TODO
    }

    private void internalRetrieveNetworkServiceEndpoints(){
        log.debug("Retrieving Network Service endpoints");
        VerticalServiceInstance vsi = verticalServiceInstanceRepo.findByVerticalServiceInstanceId(vsiId).get();
        if(vsi.getStatus()!=VerticalServiceInstanceStatus.INSTANTIATING){
            log.debug("Wrong VSI status {}. SKIPPING", vsi.getStatus());
            return;
        }
        try {
            Map<String, String> endpoints = nsLcmProvider.getNetworkServiceInstanceEndpoints(vsi.getNetworkServiceInstanceId(), vsi.getTestbed());
            vsi.setServiceEndpoints(endpoints);
            verticalServiceInstanceRepo.saveAndFlush(vsi);
        } catch (FailedOperationException e) {
            manageVsError(e.getMessage(), e);
        }

    }

    private void internalAllocateSlice(){
        log.debug("Allocating Slice for vertical service");

        Map<UUID, List<String>> allocatedEndpoints = new HashMap<>();
        VerticalServiceInstance vsi = verticalServiceInstanceRepo.findByVerticalServiceInstanceId(vsiId).get();
        if(vsi.getStatus()!=VerticalServiceInstanceStatus.INSTANTIATING){
            log.debug("Wrong VSI status {}. SKIPPING", vsi.getStatus());
            return;
        }
        String sliceId = null;

        for(ServiceEndpoint se: this.verticalServiceBlueprint.getEndPoints()){
            if(se.getSliceProfile()!=null && !se.getSliceProfile().isEmpty()){
                log.debug("Allocating Slice for vertical service endpoint:"+se.getEndpointId());
                if(se.getSliceProfile().get(0) instanceof EMBBSliceProfile){
                    log.debug("Allocating slice for EMBB slice profile");
                    try {
                       sliceId =  sliceInventoryService.allocateEMBBSlice((EMBBSliceProfile)se.getSliceProfile().get(0), vsi.getImsiIds(), vsi.getTestbed() );
                       //String  sliceInventoryService.(((EMBBSliceProfile) se.getSliceProfile().get(0)), null, vsi.getTestbed() );
                        if(allocatedEndpoints.containsKey(se.getNetAppBlueprintId())){
                            allocatedEndpoints.get(se.getNetAppBlueprintId()).add(se.getEndpointId());
                        }else{
                            List<String> neEndpoints = new ArrayList<>();
                            neEndpoints.add(se.getEndpointId());
                            allocatedEndpoints.put(se.getNetAppBlueprintId(), neEndpoints);
                        }
                        log.debug("Storing slice ID", sliceId);
                        vsi.setFiveGNetworkSliceId(sliceId);
                       verticalServiceInstanceRepo.saveAndFlush(vsi);
                    } catch (FailedOperationException e) {
                        manageVsError(e.getMessage(), e);
                    }
                }else if(se.getSliceProfile().get(0) instanceof URLLCSliceProfile){
                    log.debug("Allocating slice for URLLC slice profile");
                    try {
                        sliceId =  sliceInventoryService.allocateURLLCSlice((URLLCSliceProfile) se.getSliceProfile().get(0), vsi.getImsiIds() , vsi.getTestbed() );
                        //String  sliceInventoryService.(((EMBBSliceProfile) se.getSliceProfile().get(0)), null, vsi.getTestbed() );
                        if(allocatedEndpoints.containsKey(se.getNetAppBlueprintId())){
                            allocatedEndpoints.get(se.getNetAppBlueprintId()).add(se.getEndpointId());
                        }else{
                            List<String> neEndpoints = new ArrayList<>();
                            neEndpoints.add(se.getEndpointId());
                            allocatedEndpoints.put(se.getNetAppBlueprintId(), neEndpoints);
                        }
                        vsi.setFiveGNetworkSliceId(sliceId);
                        log.debug("Storing slice ID", sliceId);
                        verticalServiceInstanceRepo.saveAndFlush(vsi);
                    } catch (FailedOperationException e) {
                        manageVsError(e.getMessage(), e);
                    }
                }

            }
        }

        log.debug("Configuring slices for NetApp Endpoints");
        for(NetAppBlueprint nbp: netAppBlueprints){
            log.debug("Configuring slices for NetApp: "+nbp.getNetAppPackageId());
            List<String> curAllocatedEndpoints = new ArrayList<>();
            if(allocatedEndpoints.containsKey(nbp.getNetAppPackageId()))
                curAllocatedEndpoints=allocatedEndpoints.get(nbp.getNetAppPackageId());
            for(NetAppEndpoint endpoint: nbp.getEndPoints()){
                if(!curAllocatedEndpoints.contains(endpoint.getEndPointId())){
                    if(endpoint.getSliceProfile()!=null&& !endpoint.getSliceProfile().isEmpty()){
                        SliceProfile sliceProfile =endpoint.getSliceProfile().get(0);
                        try {
                            if(sliceProfile instanceof  EMBBSliceProfile){
                                    sliceId =  sliceInventoryService.allocateEMBBSlice((EMBBSliceProfile) sliceProfile, vsi.getImsiIds(), vsi.getTestbed() );
                            }else if(sliceProfile instanceof  URLLCSliceProfile){

                                    sliceId =  sliceInventoryService.allocateURLLCSlice((URLLCSliceProfile) sliceProfile, vsi.getImsiIds(), vsi.getTestbed() );

                            }
                            vsi.setFiveGNetworkSliceId(sliceId);
                            log.debug("Storing slice ID", sliceId);
                            verticalServiceInstanceRepo.saveAndFlush(vsi);
                        } catch (FailedOperationException e) {
                            manageVsError(e.getMessage(), e);
                        }

                    }
                }else log.debug("Skipping already configured NetApp Endpoint:"+endpoint.getEndPointId());
            }
        }
    }





    private void internalConfigureMonitoringMetrics()  {
        log.debug("Configuring Monitoring Metrics");

        List<InfrastructureMetricRecord> iRecords  = new ArrayList<>();
        List<ApplicationMetricRecord> aRecords = new ArrayList<>();
        log.debug("Configuring NetApp Metrics for NetApps");
        String serviceDashboardUrl = null;
        String infrastructureDashboardUrl = null;
        String networkDashboardUrl = null;
        VerticalServiceInstance vsi = verticalServiceInstanceRepo.findByVerticalServiceInstanceId(vsiId).get();

        //if(!vsi.getStatus().equals(VerticalServiceInstanceStatus.INSTANTIATING)){
        if(!(this.status == VerticalServiceInstanceStatus.INSTANTIATING)){
            log.debug("Wrong VSI status {}. SKIPPING", this.status);
            return;
        }else log.debug("VSI in {} status",this.status);
        List<InfrastructureMetric> vsInfrastructureMetrics = new ArrayList<>();
        Map<String, String> allMetricTopics = new HashMap<>();
        for(NetAppBlueprint nbp: netAppBlueprints){
            if(nbp!=null) {
                log.debug("Configuring NetApp Metrics: " + nbp.getNetAppPackageId());

                try {
                    List<InfrastructureMetric> cMetrics = nbp.getInfrastructureMetrics().stream().
                           filter(im -> im.isComputeMetric()).collect(Collectors.toList());
                    vsInfrastructureMetrics.addAll(cMetrics);
                    List<InfrastructureMetric> nMetrics = nbp.getInfrastructureMetrics().stream().
                            filter(im -> im.isNetworkMetric()).collect(Collectors.toList());
                    vsInfrastructureMetrics.addAll(nMetrics);
                    //MonitoringConfigResponse iResponse = monitoringProvider.configureInfrastructureMonitoringMetrics(this.vsiId, nbp.getInfrastructureMetrics(), vsi.getName());
                    MonitoringConfigResponse iResponse = monitoringProvider.configureInfrastructureMonitoringMetrics(this.vsiId, cMetrics, vsi.getName());
                    MonitoringConfigResponse sResponse = monitoringProvider.configureApplicationMonitoringMetrics(this.vsiId, nbp.getApplicationMetrics());
                    MonitoringConfigResponse nResponse = monitoringProvider.configureNetworkMonitoringMetrics(this.vsiId, nMetrics, vsi.getName());

                    serviceDashboardUrl=sResponse.getMonitoringDashboardUrl();
                    infrastructureDashboardUrl=iResponse.getMonitoringDashboardUrl();
                    networkDashboardUrl=nResponse.getMonitoringDashboardUrl();
                    log.debug("Received Service Monitoring Dashboard:"+serviceDashboardUrl);
                    log.debug("Received Infrastructure Monitoring Dashboard:"+infrastructureDashboardUrl);
                    log.debug("Received Network Monitoring Dashboard:"+networkDashboardUrl);
                    Map<String, String> iTopics = iResponse.getMetricTopics();
                    Map<String, String> nTopics = nResponse.getMetricTopics();
                    Map<String, String> aTopics = sResponse.getMetricTopics();
                    allMetricTopics.putAll(iTopics);
                    allMetricTopics.putAll(nTopics);
                    allMetricTopics.putAll(aTopics);
                    for (String metricId : iTopics.keySet()) {
                        InfrastructureMetricRecord imRecord =new InfrastructureMetricRecord(metricId, iTopics.get(metricId), vsi);
                        infrastructureMetricRepo.saveAndFlush(imRecord);
                        iRecords.add(imRecord);
                    }
                    for (String metricId : nTopics.keySet()) {
                        InfrastructureMetricRecord imRecord =new InfrastructureMetricRecord(metricId, nTopics.get(metricId), vsi);
                        infrastructureMetricRepo.saveAndFlush(imRecord);
                        iRecords.add(imRecord);
                    }
                    for (String metricId : aTopics.keySet()) {
                        ApplicationMetricRecord amRecord = new ApplicationMetricRecord(metricId, iTopics.get(metricId), vsi);
                        applicationMetricRepo.saveAndFlush(amRecord);
                        aRecords.add(amRecord);
                    }

                    vsi.setApplicationMetrics(aRecords);
                    vsi.setInfrastructureMetrics(iRecords);
                    verticalServiceInstanceRepo.saveAndFlush(vsi);
                } catch (FailedOperationException e) {
                    manageVsError("Failed to configure Metrics", e);
                    return;
                }
            }else log.warn("Null element in NetAppBlueprint list");

        }

        log.debug("Configuring Vertical Service Metrics and RuntimeActions");

        Map<String, String> iTopics  = null;
        Map<String, String> nTopics  = null;
        try {

            List<InfrastructureMetric> cMetrics = this.verticalServiceBlueprint.getInfrastructureMetrics().stream().
                    filter(im -> im.isComputeMetric()).collect(Collectors.toList());
            vsInfrastructureMetrics.addAll(cMetrics);
            List<InfrastructureMetric> nMetrics = this.verticalServiceBlueprint.getInfrastructureMetrics().stream().
                    filter(im -> im.isNetworkMetric()).collect(Collectors.toList());
            vsInfrastructureMetrics.addAll(nMetrics);
            MonitoringConfigResponse iResponse =
                    monitoringProvider.configureInfrastructureMonitoringMetrics(this.vsiId, cMetrics, vsi.getName());
            MonitoringConfigResponse nResponse =
                    monitoringProvider.configureNetworkMonitoringMetrics(this.vsiId, nMetrics, vsi.getName());

            iTopics = iResponse.getMetricTopics();
            nTopics= nResponse.getMetricTopics();

            allMetricTopics.putAll(iTopics);
            allMetricTopics.putAll(nTopics);
            MonitoringConfigResponse sResponse =  monitoringProvider.configureApplicationMonitoringMetrics(this.vsiId, this.verticalServiceBlueprint.getApplicationMetrics());

            Map<String, String> aTopics = sResponse.getMetricTopics();

            for(String metricId: iTopics.keySet()){
                InfrastructureMetricRecord imRecord =new InfrastructureMetricRecord(metricId, iTopics.get(metricId), vsi);
                infrastructureMetricRepo.saveAndFlush(imRecord);
                iRecords.add(imRecord);
            }

            for(String metricId: nTopics.keySet()){
                InfrastructureMetricRecord imRecord =new InfrastructureMetricRecord(metricId, iTopics.get(metricId), vsi);
                infrastructureMetricRepo.saveAndFlush(imRecord);
                iRecords.add(imRecord);
            }
            for(String metricId: aTopics.keySet()){
                ApplicationMetricRecord amRecord = new ApplicationMetricRecord(metricId, iTopics.get(metricId), vsi);
                applicationMetricRepo.saveAndFlush(amRecord);
                aRecords.add(amRecord);
            }
            vsi = verticalServiceInstanceRepo.findByVerticalServiceInstanceId(vsiId).get();
            vsi.setApplicationMetrics(aRecords);
            vsi.setInfrastructureMetrics(iRecords);
            if(iResponse.getMonitoringDashboardUrl()!=null) {
                infrastructureDashboardUrl = iResponse.getMonitoringDashboardUrl();
                log.debug("Received Infrastructure Monitoring Dashboard:" + infrastructureDashboardUrl);
            }
            if(nResponse.getMonitoringDashboardUrl()!=null) {
                networkDashboardUrl = nResponse.getMonitoringDashboardUrl();
                log.debug("Received Network Monitoring Dashboard:" + infrastructureDashboardUrl);
            }
            if(sResponse.getMonitoringDashboardUrl()!=null) {
                serviceDashboardUrl = sResponse.getMonitoringDashboardUrl();
                log.debug("Received Service Monitoring Dashboard:" + serviceDashboardUrl);
            }
            if(this.verticalServiceBlueprint.getRuntimeActions()!=null &&
                    !this.verticalServiceBlueprint.getRuntimeActions().isEmpty()){
                log.debug("Configuring RuntimeActions");
                Map<String, InfrastructureMetric> iMetricsMap = vsInfrastructureMetrics.stream()
                        .collect(Collectors.toMap(InfrastructureMetric::getMetricId, Function.identity()));
                Map<String, UUID> runtimeActions = new HashMap<>();
                for(RuntimeAction runtimeAction: this.verticalServiceBlueprint.getRuntimeActions()){
                    if(runtimeAction.getTriggerType().equals(RuntimeActionTriggerType.THRESHOLD)){
                        RuntimeThresholdAction thresholdAction = (RuntimeThresholdAction)  runtimeAction;
                        String metricTopic = allMetricTopics.get(thresholdAction.getMetricId());
                        UUID runtimeId = runtimeActionService.configureThresholdInfrastructureMetricTrigger(this.vsiId,
                                runtimeAction,
                                metricTopic,
                                iMetricsMap.get(thresholdAction.getMetricId()),
                                this

                                );
                        runtimeActions.put(runtimeAction.getRuntimeActionId(), runtimeId);
                    }else log.debug("Trigger type currently supported. Ignoring");

                    vsi.setRuntimeActions(runtimeActions);

                }
            }
            verticalServiceInstanceRepo.saveAndFlush(vsi);

        } catch (FailedOperationException e) {
            manageVsError("Error during VS Metric Configuration",e);
            return;
        }
        log.debug("Storing Service Monitoring Dashboard:"+serviceDashboardUrl);
        log.debug("Storing Infrastructure Monitoring Dashboard:"+infrastructureDashboardUrl);
        vsi.setMonitoringInfo(new MonitoringInfo(infrastructureDashboardUrl, serviceDashboardUrl, networkDashboardUrl));
        verticalServiceInstanceRepo.saveAndFlush(vsi);

    }



    void processNsiStatusChangeNotification(NotifyNsiStatusChange msg) {
        VerticalServiceInstance vsi = verticalServiceInstanceRepo.findByVerticalServiceInstanceId(this.vsiId).get();
        VerticalServiceInstanceStatus internalStatus = vsi.getStatus();
        if (!((internalStatus == VerticalServiceInstanceStatus.INSTANTIATING) || (internalStatus == VerticalServiceInstanceStatus.TERMINATING))) {
            manageVsError("Received NSI status change notification in wrong status. Skipping message.");
            return;
        }
        NetworkServiceStatusChange nsStatusChange = msg.getStatusChange();
        String nsiId = msg.getNsiId();
        try {
            switch (nsStatusChange) {
                case NS_CREATED: {
                    nsStatusChangeOperations(NetworkServiceStatus.INSTANTIATED, nsiId);
                    break;
                }

                case NS_MODIFIED: {
                    nsStatusChangeOperations(NetworkServiceStatus.MODIFIED, nsiId);
                    break;
                }
                case NS_TERMINATED: {
                    nsStatusChangeOperations(NetworkServiceStatus.TERMINATED, nsiId);
                    break;
                }
                case NS_FAILED: {
                    manageVsError("Received notification about network slice " + msg.getNsiId() + " failure");
                    break;
                }
                default:
                    break;
            }
        } catch (Exception e) {
            manageVsError("Error while processing NSI status change notification ", e);
        }
    }

    public void scaleVs(String vnfdId, int cpus, int ram){
        log.debug("Received request to scale VS");
        VerticalServiceInstance vsi = verticalServiceInstanceRepo.findByVerticalServiceInstanceId(this.vsiId).get();
        try {
            String vmReference = nsLcmProvider.getNfvoVmVimReference(vsi.getNetworkServiceInstanceId(), vnfdId, vsi.getTestbed());
            vimLcmService.scaleVM(vsi.getTestbed(), vmReference, cpus, ram);
        } catch (FailedOperationException e) {
            manageVsError("FAiled to scale VS", e);
        }
    }


    private void manageVsError(String errorMessage, Exception e) {
        log.error(errorMessage,e);
        VerticalServiceInstance vsi = verticalServiceInstanceRepo.findByVerticalServiceInstanceId(vsiId).get();
        this.updateVsiStatus(VerticalServiceInstanceStatus.FAILED);
        vsi.setErrorMsg(errorMessage);
        verticalServiceInstanceRepo.saveAndFlush(vsi);
    }

    private void manageVsError(String errorMessage) {
        log.error(errorMessage);
        VerticalServiceInstance vsi = verticalServiceInstanceRepo.findByVerticalServiceInstanceId(vsiId).get();
        this.updateVsiStatus(VerticalServiceInstanceStatus.FAILED);
        vsi.setErrorMsg(errorMessage);
        verticalServiceInstanceRepo.saveAndFlush(vsi);
    }
}
