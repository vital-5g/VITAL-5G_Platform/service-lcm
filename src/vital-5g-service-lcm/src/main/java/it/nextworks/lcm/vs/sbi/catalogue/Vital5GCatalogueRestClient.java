/*
* Copyright 2018 Nextworks s.r.l.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package it.nextworks.lcm.vs.sbi.catalogue;


import it.nextworks.catalogue.elements.*;
import it.nextworks.catalogue.elements.translator.NfvNsInstantiationInfo;
import it.nextworks.catalogue.exceptions.*;
import it.nextworks.catalogue.interfaces.NetAppPackageManagementInterface;
import it.nextworks.catalogue.interfaces.TranslatorInterface;
import it.nextworks.catalogue.interfaces.VsBlueprintCatalogueInterface;
import it.nextworks.catalogue.interfaces.VsDescriptorCatalogueInterface;
import it.nextworks.catalogue.messages.OnboardVSBRequest;
import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;


import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;


public class Vital5GCatalogueRestClient implements VsBlueprintCatalogueInterface, VsDescriptorCatalogueInterface, TranslatorInterface, NetAppPackageManagementInterface {

	private static final Logger log = LoggerFactory.getLogger(Vital5GCatalogueRestClient.class);
	
	private KeycloakRestTemplate restTemplate;


	//Added this to support the async translation of the expd
	private RestTemplate noAuthRestTemplate;
	
	private String catalogueUrl;
	
	public Vital5GCatalogueRestClient(String baseUrl, KeycloakRestTemplate restTemplate) {

	    this.catalogueUrl = baseUrl + "/portal/catalogue";
	    this.restTemplate=restTemplate;
	    this.noAuthRestTemplate= new RestTemplate(new BufferingClientHttpRequestFactory(
				new SimpleClientHttpRequestFactory()
		));
	    this.noAuthRestTemplate.setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));

	}


	@Override
	public UUID onboardVsBlueprint(OnboardVSBRequest vsb, File nsd) throws AlreadyExistingEntityException, MalformattedElementException, FailedOperationException, UnAuthorizedRequestException, NotExistingEntityException {
		return null;
	}

	@Override
	public VerticalServiceBlueprint getVerticalServiceBlueprint(UUID blueprintId) throws
			it.nextworks.catalogue.exceptions.NotExistingEntityException, FailedOperationException, MalformattedElementException, UnAuthorizedRequestException {
		log.debug("Building HTTP request for querying VS blueprint with ID " + blueprintId);
		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");
		
		HttpEntity<?> getEntity = new HttpEntity<>(header);
		
		String url = catalogueUrl + "/vsbs/" + blueprintId;

		log.debug("Sending HTTP request to retrieve VS blueprint.");

		ResponseEntity<VerticalServiceBlueprint> httpResponse =
				restTemplate.exchange(url, HttpMethod.GET, getEntity, VerticalServiceBlueprint.class);

		log.debug("Response code: " + httpResponse.getStatusCode().toString());
		HttpStatus code = httpResponse.getStatusCode();

		if (code.equals(HttpStatus.OK)) {
			log.debug("VS blueprint correctly retrieved");
			return httpResponse.getBody();
		} else if (code.equals(HttpStatus.NOT_FOUND)) {
			throw new NotExistingEntityException("Error during VS blueprint retrieval: " + httpResponse.getBody());
		} else if (code.equals(HttpStatus.FORBIDDEN)) {
			throw new it.nextworks.catalogue.exceptions.UnAuthorizedRequestException(httpResponse.getBody().toString());
		} else if (code.equals(HttpStatus.BAD_REQUEST)) {
			throw new it.nextworks.catalogue.exceptions.MalformattedElementException("Error during VS blueprint retrieval: " + httpResponse.getBody());

		} else {
			throw new it.nextworks.catalogue.exceptions.FailedOperationException("Generic error during VS LCM interaction with portal catalogue");
		}
			

	}

	@Override
	public List<VerticalServiceBlueprintInfo> queryVerticalServiceBlueprint(Testbed testbed, AccessLevel accessLevel) throws MalformattedElementException {
		return null;
	}

	@Override
	public List<VerticalServiceBlueprintInfo> getAllVerticalServiceBlueprints() {
		return null;
	}

	@Override
	public void deleteVsBlueprint(UUID vsbId, boolean force) throws FailedOperationException, UnAuthorizedRequestException, NotExistingEntityException {
		throw new FailedOperationException("Method not implemented");
	}


	@Override
	public UUID onBoardVsDescriptor(VsDescriptor request) throws MalformattedElementException, AlreadyExistingEntityException, FailedOperationException {
		throw new FailedOperationException("Method not implemented");
	}

	@Override
	public List<VsDescriptor> queryVsDescriptor() throws MalformattedElementException, FailedOperationException {
		throw new FailedOperationException("Method not implemented");
	}

	@Override
	public VsDescriptor getVsd(UUID descriptorId) throws NotExistingEntityException, MalformattedElementException, FailedOperationException {
		log.debug("Building HTTP request for querying VS Descriptor with ID " + descriptorId);
		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");
		
		HttpEntity<?> getEntity = new HttpEntity<>(header);
		
		String url = catalogueUrl + "/vsds/" + descriptorId;
		try {
			log.debug("Sending HTTP request to retrieve VS descriptor.");
			
			ResponseEntity<VsDescriptor> httpResponse =
					restTemplate.exchange(url, HttpMethod.GET, getEntity, VsDescriptor.class);
			
			log.debug("Response code: " + httpResponse.getStatusCode().toString());
			HttpStatus code = httpResponse.getStatusCode();
			
			if (code.equals(HttpStatus.OK)) {
				log.debug("VS descriptor correctly retrieved");
				return httpResponse.getBody();
			} else if (code.equals(HttpStatus.NOT_FOUND)) {
				throw new NotExistingEntityException("Error during VS descriptor retrieval: " + httpResponse.getBody());
			} else if (code.equals(HttpStatus.BAD_REQUEST)) {
				throw new MalformattedElementException("Error during VS descriptor retrieval: " + httpResponse.getBody());
			} else if (code.equals(HttpStatus.FORBIDDEN)) {
				throw new it.nextworks.catalogue.exceptions.UnAuthorizedRequestException(httpResponse.getBody().toString());
			} else {
				throw new FailedOperationException("Generic error during VS LCM interaction with portal catalogue");
			}
			
		} catch (Exception e) {
			log.debug("Error while interacting with portal catalogue.");
			throw new FailedOperationException("Error while interacting with portal catalogue at URL " + url);
		}
	}

	@Override
	public void deleteVsDescriptor(UUID vsDescriptorId, boolean force) throws MalformattedElementException, NotExistingEntityException, FailedOperationException {

	}

	@Override
	public NfvNsInstantiationInfo translateVsd(UUID vsdId)
			throws NotExistingEntityException, FailedOperationException {
		log.debug("Building HTTP request for translation of Vertical Service Descriptor with ID " + vsdId);
		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");
		
		HttpEntity<?> getEntity = new HttpEntity<>(header);
		
		String url = catalogueUrl + "/translator/" + vsdId;

		try {
			log.debug("Sending HTTP request to retrieve vertical service descriptor translation into NFV NS.");
			
			ResponseEntity<NfvNsInstantiationInfo> httpResponse =
					restTemplate.exchange(url, HttpMethod.GET, getEntity, NfvNsInstantiationInfo.class);
			
			log.debug("Response code: " + httpResponse.getStatusCode().toString());
			HttpStatus code = httpResponse.getStatusCode();
			
			if (code.equals(HttpStatus.OK)) {
				log.debug("Vertical descriptor translation correctly retrieved");
				return httpResponse.getBody();
			} else if (code.equals(HttpStatus.NOT_FOUND)) {
				throw new NotExistingEntityException("Error during Vertical descriptor translation: " + httpResponse.getBody());
			} else if (code.equals(HttpStatus.BAD_REQUEST)) {
				throw new MalformattedElementException("Error during Vertical descriptor translation: " + httpResponse.getBody());

			} else if (code.equals(HttpStatus.FORBIDDEN)) {
				throw new it.nextworks.catalogue.exceptions.UnAuthorizedRequestException(httpResponse.getBody().toString());
			} else {
				throw new FailedOperationException("Error while interacting with portal catalogue at url " + url);
			}
			
		} catch (Exception e) {
			log.debug("Error while interacting with portal catalogue.");
			throw new FailedOperationException("Error while interacting with portal catalogue at url " + url);
		}
	}

	@Override
	public void useVsDescriptor(UUID vsdId, UUID vsiId) throws FailedOperationException {

		log.debug("Building HTTP request for use of Vertical Service Descriptor with ID " + vsdId);
		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");

		HttpEntity<?> getEntity = new HttpEntity<>(header);

		String url = catalogueUrl + "/vsdescriptor/" +vsdId+"/use/"+vsiId;
		try {
			log.debug("Sending HTTP request to retrieve vertical service descriptor translation into NFV NS.");

			ResponseEntity<String> httpResponse =
					restTemplate.exchange(url, HttpMethod.POST, getEntity, String.class);

			log.debug("Response code: " + httpResponse.getStatusCode().toString());
			HttpStatus code = httpResponse.getStatusCode();

			if (code.equals(HttpStatus.OK)) {
				log.debug("Vertical Service descriptor correctly locked from removal");

			} else if (code.equals(HttpStatus.NOT_FOUND)) {
				throw new NotExistingEntityException("Error during vertical service descriptor translation: " + httpResponse.getBody());
			} else if (code.equals(HttpStatus.BAD_REQUEST)) {
				throw new MalformattedElementException("Error during vertical service descriptor translation: " + httpResponse.getBody());

			} else if (code.equals(HttpStatus.FORBIDDEN)) {
				throw new it.nextworks.catalogue.exceptions.UnAuthorizedRequestException(httpResponse.getBody().toString());
			} else {
				throw new FailedOperationException("Generic error during VS LCM interaction with portal catalogue");
			}

		} catch (Exception e) {
			log.debug("Error while interacting with portal catalogue.");
			throw new FailedOperationException("Error while interacting with portal catalogue at url " + url);
		}

	}

	@Override
	public void releaseVsDescriptor(UUID vsdId, UUID vsiId) throws FailedOperationException {

		log.debug("Building HTTP request for use of vertical service Descriptor with ID " + vsdId);
		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");

		HttpEntity<?> getEntity = new HttpEntity<>(header);

		String url = catalogueUrl + "/vsdescriptor/" +vsdId+"/release/"+vsiId;
		try {
			log.debug("Sending HTTP request to release the vertical service descriptor:"+vsdId+" from service:"+vsiId);

			ResponseEntity<String> httpResponse =
					restTemplate.exchange(url, HttpMethod.POST, getEntity, String.class);

			log.debug("Response code: " + httpResponse.getStatusCode().toString());
			HttpStatus code = httpResponse.getStatusCode();

			if (code.equals(HttpStatus.OK)) {
				log.debug("Vertical service descriptor released correctly");

			} else if (code.equals(HttpStatus.NOT_FOUND)) {
				throw new NotExistingEntityException(httpResponse.getBody());
			} else if (code.equals(HttpStatus.BAD_REQUEST)) {
				throw new MalformattedElementException(httpResponse.getBody());

			} else if (code.equals(HttpStatus.FORBIDDEN)) {
				throw new it.nextworks.catalogue.exceptions.UnAuthorizedRequestException(httpResponse.getBody().toString());
			} else {
				throw new FailedOperationException("Generic error during VS LCM interaction with portal catalogue");
			}

		} catch (Exception e) {
			log.debug("Error while interacting with portal catalogue.");
			throw new FailedOperationException("Error while interacting with portal catalogue at url " + url);
		}

	}

	@Override
	public UUID onboardNetAppPackage(File file) throws AlreadyExistingEntityException, MalformattedElementException, UnAuthorizedRequestException, FailedOperationException {
		return null;
	}

	@Override
	public UUID onboardNetAppPackage(File file, File file1) throws AlreadyExistingEntityException, MalformattedElementException, UnAuthorizedRequestException, FailedOperationException {
		return null;
	}

	@Override
	public NetAppBlueprint getNetAppBlueprint(UUID uuid) throws UnAuthorizedRequestException, NotExistingEntityException, FailedOperationException {
		log.debug("Building HTTP request for use of NetAppBlueprint with ID " + uuid);
		HttpHeaders header = new HttpHeaders();
		header.add("Content-Type", "application/json");

		HttpEntity<?> getEntity = new HttpEntity<>(header);

		String url = catalogueUrl + "/netapppackages/" +uuid+"/blueprint";
		log.debug("Sending HTTP request to retrieve the NetAppBlueprint");

		ResponseEntity<NetAppBlueprint> httpResponse =
				restTemplate.exchange(url, HttpMethod.GET, getEntity, NetAppBlueprint.class);

		log.debug("Response code: " + httpResponse.getStatusCode().toString());
		HttpStatus code = httpResponse.getStatusCode();

		if (code.equals(HttpStatus.OK)) {
			log.debug("NetAppBlueprint correctly retrieved");
			return httpResponse.getBody();
		}else if(code.equals(HttpStatus.NOT_FOUND)){

			throw new NotExistingEntityException(httpResponse.getBody().toString());
		}else if(code.equals(HttpStatus.FORBIDDEN)){
			throw new UnAuthorizedRequestException(httpResponse.getBody().toString());
		}else{
			throw new FailedOperationException("Error during NetApp Blueprint retrieval from catalogue");
		}

	}

	@Override
	public File getNetAppPackage(UUID uuid) throws UnAuthorizedRequestException, NotExistingEntityException {
		return null;
	}

	@Override
	public List<NetAppPackageInfo> queryNetAppPackages(Testbed testbed, AccessLevel accessLevel, NetAppSpecLevel netAppSpecLevel) {
		return null;
	}

	@Override
	public List<NetAppPackageInfo> getAllNetAppPackages() {
		return null;
	}

	@Override
	public void deleteNetAppPackage(UUID uuid, boolean b) throws UnAuthorizedRequestException, NotExistingEntityException {

	}

	@Override
	public File getNetAppPackageSoftwareDoc(UUID uuid, String s) throws UnAuthorizedRequestException, NotExistingEntityException, FailedOperationException {
		return null;
	}
}
