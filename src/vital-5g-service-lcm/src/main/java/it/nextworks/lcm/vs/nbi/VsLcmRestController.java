package it.nextworks.lcm.vs.nbi;

import java.util.*;
import java.util.stream.Collectors;


import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.nextworks.lcm.vs.VsLcmService;
import it.nextworks.lcm.vs.elements.Testbed;
import it.nextworks.lcm.vs.elements.VerticalServiceInstance;
import it.nextworks.lcm.vs.elements.VerticalServiceInstanceInfo;
import it.nextworks.lcm.vs.exceptions.MalformattedElementException;
import it.nextworks.lcm.vs.exceptions.NotExistingEntityException;
import it.nextworks.lcm.vs.messages.InstantiateVsRequest;
import it.nextworks.lcm.vs.messages.OnboardRuntimeActionRequest;
import it.nextworks.lcm.vs.messages.ScaleVsRequest;
import it.nextworks.lcm.vs.messages.VsExperimentAllocationRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



@RestController
@CrossOrigin
@RequestMapping("/portal/vslcm")
public class VsLcmRestController {

	private static final Logger log = LoggerFactory.getLogger(VsLcmRestController.class);
	
	@Autowired
	private VsLcmService engine;


	
	public VsLcmRestController() { }

	@ApiOperation(value = "Instantiate Vertical Service")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "The ID of the created vertical service instance.", response = UUID.class),
			//@ApiResponse(code = 400, message = "The request contains elements impossible to process", response = ResponseEntity.class),
			//@ApiResponse(code = 409, message = "There is a conflict with the request", response = ResponseEntity.class),
			//@ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)

	})
	@ResponseStatus(HttpStatus.CREATED)

	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<?> instantiateVs(@RequestBody InstantiateVsRequest request) {
		log.debug("Received request to instantiate a VS.");
		
		//TODO: To be improved once the final authentication platform is in place.


		try {
			UUID vsiId = engine.instantiateVs(request);
			return new ResponseEntity<UUID>(vsiId, HttpStatus.CREATED);
		} catch (NotExistingEntityException e) {
			log.error("Something not found: " + e.getMessage(),e);
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (MalformattedElementException e) {
			log.error("Malformatted request: " + e.getMessage(),e);
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			log.error("Internal exception: " + e.getMessage(), e);
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ApiOperation(value = "Use Vertical Service")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "The ID of the created vertical service instance.", response = UUID.class),
			//@ApiResponse(code = 400, message = "The request contains elements impossible to process", response = ResponseEntity.class),
			//@ApiResponse(code = 409, message = "There is a conflict with the request", response = ResponseEntity.class),
			//@ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)

	})
	@ResponseStatus(HttpStatus.CREATED)

	@RequestMapping(value = "/{vsiId}/use", method = RequestMethod.POST)
	public ResponseEntity<?> useVsi(@PathVariable UUID vsiId, @RequestBody VsExperimentAllocationRequest request) {
		log.debug("Received request to use a VS.");

		//TODO: To be improved once the final authentication platform is in place.


		try {
			engine.useVs(vsiId, UUID.fromString(request.getExperimentExecutionId()));
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (NotExistingEntityException e) {
			log.error("Something not found: " + e.getMessage(),e);
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (MalformattedElementException e) {
			log.error("Malformatted request: " + e.getMessage(),e);
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			log.error("Internal exception: " + e.getMessage(), e);
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ApiOperation(value = "Release Vertical Service")
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "The ID of the created vertical service instance.", response = UUID.class),
			//@ApiResponse(code = 400, message = "The request contains elements impossible to process", response = ResponseEntity.class),
			//@ApiResponse(code = 409, message = "There is a conflict with the request", response = ResponseEntity.class),
			//@ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)

	})
	@ResponseStatus(HttpStatus.CREATED)

	@RequestMapping(value = "/{vsiId}/release", method = RequestMethod.POST)
	public ResponseEntity<?> releaseVsi(@PathVariable UUID vsiId, @RequestBody VsExperimentAllocationRequest request) {
		log.debug("Received request to release a VS.");

		//TODO: To be improved once the final authentication platform is in place.


		try {
			engine.releaseVs(vsiId, UUID.fromString(request.getExperimentExecutionId()));
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (NotExistingEntityException e) {
			log.error("Something not found: " + e.getMessage(),e);
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
		} catch (MalformattedElementException e) {
			log.error("Malformatted request: " + e.getMessage(),e);
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			log.error("Internal exception: " + e.getMessage(), e);
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	@ApiOperation(value = "Get Vertical Service Instances")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "List of Vertical Service Instances.", response = VerticalServiceInstance.class, responseContainer = "Set"),
			//@ApiResponse(code = 400, message = "The request contains elements impossible to process", response = ResponseEntity.class),
			//@ApiResponse(code = 409, message = "There is a conflict with the request", response = ResponseEntity.class),
			//@ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)

	})
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<?> getAllVS(@RequestParam(required = false) String tenant, @RequestParam(required = false) Testbed testbed,
									  @RequestParam(required = false) String usecase, @RequestParam(required = false) UUID vsbId,
									  @RequestParam(required = false) UUID vsdId ) {
		log.debug("Received request to retrieve info about vertical service instances.");

		
		try {

			List<VerticalServiceInstanceInfo> experiments = engine.queryVs(tenant, testbed, usecase, vsbId, vsdId);
			return new ResponseEntity<List<VerticalServiceInstanceInfo>>(experiments, HttpStatus.OK);
		} catch (MalformattedElementException e) {
			log.error("Malformatted request: " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			log.error("Internal exception: " + e.getMessage());
			log.debug("Internal exception: ", e);
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ApiOperation(value = "Get Vertical Service Instance")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Vertical Service Instances.", response = VerticalServiceInstance.class)
	})
	@ResponseStatus(HttpStatus.NO_CONTENT)

	@RequestMapping(value = "/{vsiId}", method = RequestMethod.GET)
	public ResponseEntity<?> getVerticalServiceInstance(@PathVariable UUID vsiId) {
		log.debug("Received request to retrieve vertical service instance " + vsiId);

		try {
			VerticalServiceInstance vsi =  engine.getVerticalServiceInstance(vsiId);
			return new ResponseEntity<VerticalServiceInstance>(vsi, HttpStatus.OK);
		} catch (MalformattedElementException e) {
			log.error("Malformatted request: " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		} catch (NotExistingEntityException e) {
			log.error("Experiment not found: " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			log.error("Internal exception: " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}





	@ApiOperation(value = "Delete Vertical Service Instance")
	@ApiResponses(value = {

	})
	@ResponseStatus(HttpStatus.NO_CONTENT)
	
	@RequestMapping(value = "/{vsiId}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteVerticalServiceInstance(@PathVariable UUID vsiId,
														   @RequestParam(required = false) boolean forced) {
		log.debug("Received request to delete vertical service instance " + vsiId);

		try {
			engine.deleteVs(vsiId, forced);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (MalformattedElementException e) {
			log.error("Malformatted request: " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		} catch (NotExistingEntityException e) {
			log.error("Experiment not found: " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			log.error("Internal exception: " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ApiOperation(value = "Terminate Vertical Service Instance")
	@ApiResponses(value = {

	})
	@ResponseStatus(HttpStatus.NO_CONTENT)

	@RequestMapping(value = "/{vsiId}/terminate", method = RequestMethod.POST)
	public ResponseEntity<?> terminateVerticalServiceInstance(@PathVariable UUID vsiId) {
		log.debug("Received request to terminate vertical service instance " + vsiId);

		try {
			engine.terminateVs(vsiId);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (MalformattedElementException e) {
			log.error("Malformatted request: " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		} catch (NotExistingEntityException e) {
			log.error("Experiment not found: " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			log.error("Internal exception: " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{vsiId}/scale", method = RequestMethod.POST)
	public ResponseEntity<?> scaleVerticalServiceInstance(@PathVariable UUID vsiId, @RequestBody ScaleVsRequest request) {
		log.debug("Received request to scale vertical service instance " + vsiId);

		try {
			engine.scaleVs(request);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (MalformattedElementException e) {
			log.error("Malformatted request: " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		} catch (NotExistingEntityException e) {
			log.error("Experiment not found: " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			log.error("Internal exception: " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{vsiId}/runtime", method = RequestMethod.POST)
	public ResponseEntity<?> onboardRuntime(@PathVariable UUID vsiId, @RequestBody OnboardRuntimeActionRequest request) {
		log.debug("Received request to add runtime to vertical service instance " + vsiId);

		try {
			engine.onboardRuntimeAction(vsiId, request);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);

		} catch (NotExistingEntityException e) {
			log.error("Experiment not found: " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			log.error("Internal exception: " + e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
