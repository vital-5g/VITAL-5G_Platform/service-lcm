package it.nextworks.lcm.vs.sbi.license;

import java.util.Map;
import java.util.UUID;

public class ServiceLicenseValidationResult {


    private boolean successful;
    private String message;

    // Map netapp id - license id
    private Map<UUID, UUID> netappLicenses;

    public ServiceLicenseValidationResult(boolean successful, String message, Map<UUID, UUID> netappLicenses) {
        this.successful = successful;
        this.message = message;
        this.netappLicenses = netappLicenses;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public String getMessage() {
        return message;
    }

    public Map<UUID, UUID> getNetappLicenses() {
        return netappLicenses;
    }
}
