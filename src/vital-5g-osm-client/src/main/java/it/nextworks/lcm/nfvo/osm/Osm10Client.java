package it.nextworks.lcm.nfvo.osm;

import com.squareup.okhttp.logging.HttpLoggingInterceptor;
import it.nextworks.inventory.elements.TestbedNfvoInformation;
import it.nextworks.lcm.nfvo.osm.rest.api.NsInstancesApi;
import it.nextworks.lcm.nfvo.osm.rest.api.NsPackagesApi;
import it.nextworks.lcm.nfvo.osm.rest.auth.OAuthSimpleClient;
import it.nextworks.lcm.nfvo.osm.rest.client.ApiClient;
import it.nextworks.lcm.nfvo.osm.rest.client.ApiException;
import it.nextworks.lcm.nfvo.osm.rest.model.*;
import it.nextworks.lcm.vs.elements.Testbed;
import it.nextworks.lcm.vs.exceptions.FailedOperationException;
import it.nextworks.lcm.vs.interfaces.nfvo.NsLcmProviderInterface;

import it.nextworks.lcm.vs.interfaces.nfvo.messages.CreateNsIdentifierRequestInternal;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.InstantiateNsRequestInternal;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.OperationStatus;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.ScaleNsRequestInternal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class Osm10Client implements NsLcmProviderInterface {
    private static final Logger log = LoggerFactory.getLogger(Osm10Client.class);

    private NsInstancesApi nsInstancesApi;
    private NsPackagesApi nsPackagesApi;
    private TestbedNfvoInformation testbedNfvoInformation;
    private final OAuthSimpleClient oAuthSimpleClient;


    public Osm10Client(TestbedNfvoInformation info){

        nsInstancesApi = new NsInstancesApi();
        nsPackagesApi = new NsPackagesApi();
        this.testbedNfvoInformation=info;
        this.oAuthSimpleClient = new OAuthSimpleClient(info.getBaseUrl()+"/admin/v1/tokens",
                info.getUsername(),
                info.getPassword(),
                info.getProject());

    }

    @Override
    public String createNetworkServiceIdentifier(CreateNsIdentifierRequestInternal request) throws FailedOperationException {
        log.debug("Received request to create a network service identifier");
        ApiClient apiClient = getClient();
        nsInstancesApi.setApiClient(apiClient);
        nsPackagesApi.setApiClient(apiClient);
        InstantiateNsRequest osmReq = new InstantiateNsRequest();
        osmReq.setVimAccountId(testbedNfvoInformation.getVimAccountId());

        osmReq.setNsName(request.getNsName());

        try {

            UUID osmId = getIdForNsdId(request.getNsdId());
            osmReq.setNsdId(osmId);
            return nsInstancesApi.addNSinstance(osmReq).getId().toString();


        } catch (ApiException e) {
            log.error("Error while creating NS Identifier", e);
          throw new FailedOperationException(e);
        }

    }

    @Override
    public String instantiateNetworkService(InstantiateNsRequestInternal request) throws FailedOperationException {
        log.debug("Received request to create a network service identifier");
        ApiClient apiClient = getClient();
        nsInstancesApi.setApiClient(apiClient);
        nsPackagesApi.setApiClient(apiClient);
        InstantiateNsRequest osmReq = new InstantiateNsRequest();
        osmReq.setAdditionalParamsForNs(request.getAdditionalParamForNs());
        osmReq.setNsName(request.getNsName());
        osmReq.setVimAccountId(testbedNfvoInformation.getVimAccountId());
        try {
            UUID osmNsdId = getIdForNsdId(request.getNsdId());
            Nsd nsd = nsPackagesApi.getNSDescriptor(osmNsdId.toString());
            for(VirtualLinkDesc vl: nsd.getVirtualLinkDesc()){
                if(vl.getId().contains("ext")){
                    log.debug("mapping vl:"+vl.getId()+"to external network:"+testbedNfvoInformation.getVimExternalNetwork());
                    InstantiateNsRequestVld vldReq = new InstantiateNsRequestVld();
                    vldReq.setName(vl.getId());
                    vldReq.setVimNetworkName(testbedNfvoInformation.getVimExternalNetwork());
                    osmReq.addVldItem(vldReq);
                }
            }
            osmReq.setNsdId(osmNsdId);
            String operationId = nsInstancesApi.instantiateNSinstance(request.getNsInstanceId(), osmReq).getId().toString();
            //pollingManager.addOperation(operationId, OperationStatus.SUCCESSFULLY_DONE, request.getNsInstanceId(), NfvoLcmOperationType.NS_INSTANTIATION, request.getTestbed());
            return operationId;
        } catch (ApiException e) {
            log.error("Error while instantiating an NS Identifier", e);
            throw new FailedOperationException(e);
        }
    }

    public String scaleNetworkService(ScaleNsRequestInternal request) throws FailedOperationException {
        log.debug("Received request to SCALE network service {}", request.getNsInstanceId());
        ApiClient apiClient = getClient();
        nsInstancesApi.setApiClient(apiClient);
        nsPackagesApi.setApiClient(apiClient);
        InstantiateNsRequest osmReq = new InstantiateNsRequest();
        osmReq.setAdditionalParamsForNs(request.getAdditionalParamForNs());

        try {


            String operationId = nsInstancesApi.scaleNSinstance(request.getNsInstanceId(), new ScaleNsRequest()).toString();

            //pollingManager.addOperation(operationId, OperationStatus.SUCCESSFULLY_DONE, request.getNsInstanceId(), NfvoLcmOperationType.NS_INSTANTIATION, request.getTestbed());
            return operationId;
        } catch (ApiException e) {
            log.error("Error while instantiating an NS Identifier", e);
            throw new FailedOperationException(e);
        }
    }

    @Override
    public OperationStatus getOperationStatus(String operationId, Testbed testbed) throws FailedOperationException {
        log.debug("Received request to query a network service operation");
        nsInstancesApi.setApiClient(getClient());
        try {
            NsLcmOpOcc opResult = nsInstancesApi.getNSLCMOpOcc(operationId);
            OsmNsLcmOperationStatus  osmOperationStatus = OsmNsLcmOperationStatus.valueOf(opResult.getOperationState());

            if (OsmNsLcmOperationStatus.FAILED == osmOperationStatus || OsmNsLcmOperationStatus.FAILED_TEMP == osmOperationStatus) {
                return OperationStatus.FAILED;
            } else if (OsmNsLcmOperationStatus.COMPLETED==osmOperationStatus || osmOperationStatus==OsmNsLcmOperationStatus.PARTIALLY_COMPLETED) {
                return OperationStatus.SUCCESSFULLY_DONE;
            } else if (osmOperationStatus==OsmNsLcmOperationStatus.ROLLING_BACK || osmOperationStatus==OsmNsLcmOperationStatus.ROLLED_BACK) {
                //TODO: See implications
                return OperationStatus.FAILED;
            } else if (osmOperationStatus==OsmNsLcmOperationStatus.PROCESSING) {
                return OperationStatus.PROCESSING;
            }else return null;

        } catch (ApiException e) {
            log.error("Error while creating NS Identifier", e);
            throw new FailedOperationException(e);
        }
    }

    @Override
    public Map<String, String> getNetworkServiceInstanceEndpoints(String nsInstanceId, Testbed testbed) throws FailedOperationException {
       log.debug("Received request to retrieve instance endpoints:"+nsInstanceId);
       nsInstancesApi.setApiClient(getClient());

        try {
            NsInstance nsInstance = nsInstancesApi.getNSinstance(nsInstanceId, false);
            log.debug("Retrieving VNF Instance information");
            List<String> vnfInstanceRef = nsInstance.getConstituentVnfrRef();
            Map<String, String> endpoints = new HashMap<>();
            if(vnfInstanceRef!=null && !vnfInstanceRef.isEmpty()){
                for(String vnfInstanceId: vnfInstanceRef){
                    log.debug("Retrieving VNF instance info for:"+vnfInstanceId);
                    VnfInstanceInfo vnfInstanceInfo = nsInstancesApi.getVnfInstance(vnfInstanceId);
                    List<Vdur> vdurs = vnfInstanceInfo.getVdur();
                    if(vdurs!=null&& !vdurs.isEmpty()){
                        for(Vdur vdu : vdurs) {
                            List<VnfInterface> vnfInterfaces = vdu.getInterfaces();
                            if(vnfInterfaces!=null&&!vnfInterfaces.isEmpty()){
                                for(VnfInterface vnfInterface: vnfInterfaces){
                                    log.debug("Retrieve address for interface:{} {} ",vnfInterface.getName(), vnfInterface.getIpAddress());
                                    endpoints.put(vnfInterface.getName(), vnfInterface.getIp_address());
                                }

                            }else log.debug("Empty list of VDU interfaces");
                        }
                    }else log.debug("Empty list of VDUs");


                }
            }else log.debug("Empty list of VNF instances");
            return endpoints;
        } catch (ApiException e) {
            log.error("Error during instance retrieval from OSM", e);
            throw new FailedOperationException("Error while interfacing with OSM in testbed "+ testbed+". Please contact a Platform admin");
        }



    }

    @Override
    public String terminateNetworkService(String networkServiceInstanceId, Testbed testbed) throws FailedOperationException {
        log.debug("Received request to terminate network service:"+networkServiceInstanceId);
        nsInstancesApi.setApiClient(getClient());

        try {
            return nsInstancesApi.terminateNSinstance(networkServiceInstanceId, null).getId().toString();
        } catch (ApiException e) {
            log.error("Error while terminating NS", e);
            throw new FailedOperationException(e);
        }
    }

    @Override
    public String executeDay2Action(String nsInstanceId, String actionId,String nfvoRef, Map<String, String> inputParams, Testbed testbed) throws FailedOperationException {
        log.debug("Received request to execute day2 action on NS instance {} - {}", nsInstanceId, nfvoRef);
        nsInstancesApi.setApiClient(getClient());
        try{

            NSinstanceActionRequest body = new NSinstanceActionRequest();
            body.setPrimitive(actionId);
            String vnfProfileId= nfvoRef.split("\\.")[0];
            String vduId= nfvoRef.split("\\.")[1];
            body.setMemberVnfIndex(vnfProfileId);
            KeyValuePairs values=new KeyValuePairs();
            if(inputParams!=null) {
                values.putAll(inputParams);
            }
            body.setVduId(vduId);
            body.setPrimitiveParams(values);
            ObjectId operationId = nsInstancesApi.actionOnNSinstance(nsInstanceId, body);
            log.debug("Received operation ID {}", operationId.getId());
            return operationId.getId().toString();
        } catch (ApiException e) {
            log.error("Error during OSM DAY2 execution", e);
            throw new FailedOperationException("Error during OSM DAY2 execution. Please contact a Platform administrator");
        }

    }


    @Override
    public String getNfvoVmVimReference(String nsInstanceId, String vnfReference, Testbed testbed) throws FailedOperationException{
        log.debug("Received request to retrieve NFVO VM VIM Id instance: {} vnfReference: {} testbed:{}", nsInstanceId, vnfReference, testbed);
        nsInstancesApi.setApiClient(getClient());
        String[] res = vnfReference.split("[.]", 0);
        String vnfdId = res[0];
        String vduId = null;
        if(res.length>1){
            vduId=res[1];
        }
        try {
            NsInstance nsInstance = nsInstancesApi.getNSinstance(nsInstanceId, false);
            List<String> vnfInstanceRef = nsInstance.getConstituentVnfrRef();

            if(vnfInstanceRef!=null && !vnfInstanceRef.isEmpty()){
                for(String vnfInstanceId: vnfInstanceRef){
                    log.debug("Retrieving VNF instance info for:"+vnfInstanceId);
                    VnfInstanceInfo vnfInstanceInfo = nsInstancesApi.getVnfInstance(vnfInstanceId);
                    log.debug("Retrieved instance VNFD ID: {} ({})", vnfInstanceInfo.getVnfdId(), vnfdId);
                    if(vnfInstanceInfo.getVnfdRef()!=null && vnfInstanceInfo.getVnfdRef().equals(vnfdId)){
                        List<Vdur> vdurs = vnfInstanceInfo.getVdur();
                        if(vdurs!=null&& !vdurs.isEmpty()) {
                            if (vduId == null) {
                                log.debug("No VDU specified, using first one");
                                return vdurs.get(0).getVimId();
                            }else{
                                for(Vdur vdu : vdurs) {
                                    log.debug("Found VDU {}", vdu.getVduIdRef());
                                    if(vdu.getVduIdRef().equals(vduId)){

                                        return vdu.getVimId();
                                    }
                                }
                            }
                        }else return null;



                    }



                }
                log.debug("Could not find VNF/VDU.");
                return null;
            }else{
                log.debug("Empty list of VNF instances");
                return null;
            }
        } catch (ApiException e) {
            log.error("Error during instance retrieval from OSM", e);
            throw new FailedOperationException("Error while interfacing with OSM in testbed "+ testbed+". Please contact a Platform admin");
        }
    }
    private ApiClient getClient() throws FailedOperationException {

        ApiClient apiClient = new ApiClient();
        apiClient.setHttpClient(OAuthSimpleClient.getUnsafeOkHttpClient());
        apiClient.setBasePath(this.testbedNfvoInformation.getBaseUrl());
        apiClient.setUsername(this.testbedNfvoInformation.getUsername());
        apiClient.setPassword(this.testbedNfvoInformation.getPassword());
        apiClient.setAccessToken(oAuthSimpleClient.getToken());
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        apiClient.getHttpClient().interceptors().add(interceptor);
        apiClient.setConnectTimeout(0);
        apiClient.setWriteTimeout(0);
        apiClient.setReadTimeout(0);
        apiClient.setDebugging(true);
        return apiClient;
    }

    private UUID getIdForNsdId(String nsdId) throws FailedOperationException {
        log.debug("Getting ID for NSD_IO: "+nsdId);
        ApiClient apiClient = getClient();
        nsPackagesApi.setApiClient(apiClient);
        ArrayOfNsdInfo nsdInfos = null;
        try{
            nsdInfos= nsPackagesApi.getNSDs();
        }catch (Exception e){
            log.error("Error querying OSM:", e);
            throw new FailedOperationException("Failed to retrieve NSD Infos from OSM. Contact a Platform administrator");
        }

        if(nsdInfos!=null || nsdInfos.isEmpty()){

            for(NsdInfo nsdInfo : nsdInfos){
                log.debug("OSM NSD Info ID"+nsdInfo.getIdentifier());
                if(nsdInfo.getIdentifier()!=null && nsdInfo.getIdentifier().equals(nsdId)){
                    UUID osmId = nsdInfo.getId();
                    log.debug("Retrieved Info ID from OSM:"+osmId);
                    return osmId;
                }
            }
            throw new FailedOperationException("Could not find UUID for NSD ID from OSM. No NSD Info matching NSD ID:"+nsdId);
        }else  throw new FailedOperationException("Could not find UUID for NSD ID. Empty NSD Info list received from OSM");



    }
}
