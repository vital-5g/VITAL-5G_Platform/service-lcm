package it.nextworks.lcm.vs.elements;

public enum AccessLevel {

    PRIVATE,
    RESTRICTED,
    PUBLIC
}
