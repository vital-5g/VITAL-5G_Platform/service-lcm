package it.nextworks.lcm.vs.interfaces.nfvo.messages;

import it.nextworks.lcm.vs.elements.Testbed;

public class InternalNsLifecycleChangeNotification

{

        private String operationId;
        private String nsInstanceId;
        private Testbed testbed;
        private NfvoLcmNotificationType lcmNotificationType;
        //private String operation;
        private NfvoLcmOperationType operationType;

    public InternalNsLifecycleChangeNotification(String operationId, String nsInstanceId, Testbed testbed, NfvoLcmNotificationType lcmNotificationType, NfvoLcmOperationType operationType) {
        this.operationId = operationId;
        this.nsInstanceId = nsInstanceId;
        this.testbed = testbed;
        this.lcmNotificationType = lcmNotificationType;
        //this.operation=operation;
        this.operationType =operationType;
    }

    public NfvoLcmOperationType getOperationType() {
        return operationType;
    }

    public String getOperationId() {
        return operationId;
    }

    public String getNsInstanceId() {
        return nsInstanceId;
    }

    public Testbed getTestbed() {
        return testbed;
    }

    public NfvoLcmNotificationType getLcmNotificationType() {
        return lcmNotificationType;
    }
}
