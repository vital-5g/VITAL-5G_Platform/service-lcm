/*
* Copyright 2018 Nextworks s.r.l.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package it.nextworks.lcm.vs.messages;

import java.util.*;

import com.fasterxml.jackson.annotation.JsonInclude;
import it.nextworks.lcm.vs.elements.AccessLevel;
import it.nextworks.lcm.vs.exceptions.MalformattedElementException;


/**
 * Request to instantiate a new Vertical Service instance.
 * 
 * @author nextworks
 *
 */
public class InstantiateVsRequest implements InterfaceMessage {

	private String name;
	private String description;
	private UUID vsdId;
	private String tenantId;

	private Map<String, String> configurationParameters = new HashMap<>();
	private Map<String, String> serviceParameters = new HashMap<>();
	private List<String> imsiIds = new ArrayList<>();
	private Map<String, String> licenseKeys= new HashMap<>();
	private AccessLevel accessLevel;

    public InstantiateVsRequest() {
    }


	/**
	 * Constructor
	 * 
	 * @param name Name of the Vertical Service instance
	 * @param description Description of the Vertical Service instance 
	 * @param vsdId ID of the VSD to be used to instantiate the Vertical Service instance
	 * @param tenantId ID of the tenant instantiating the Vertical Service instance
	 * @param configurationParameters Additional data, like config parameters provided by the vertical

	 * 
	 */
	public InstantiateVsRequest(String name, String description, UUID vsdId, String tenantId,
								Map<String, String> configurationParameters, Map<String,String > serviceParameters, List<String> imsiIds,
								AccessLevel accessLevel, Map<String, String> licenseKeys ) {
		this.name = name;
		this.description = description;
		this.vsdId = vsdId;
		this.tenantId = tenantId;
		this.accessLevel=accessLevel;
		if (configurationParameters != null) this.configurationParameters = configurationParameters;
		if (serviceParameters!=null) this.serviceParameters=serviceParameters;
		if (imsiIds!=null) this.imsiIds = imsiIds;
		if (licenseKeys!=null) this.licenseKeys= licenseKeys;
	}

	public AccessLevel getAccessLevel() {
		return accessLevel;
	}


	public Map<String, String> getLicenseKeys() {
		return licenseKeys;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * @return the vsdId
	 */
	public UUID getVsdId() {
		return vsdId;
	}


	/**
	 * @return the tenantId
	 */
	public String getTenantId() {
		return tenantId;
	}





	/**
	 * @return the userData
	 */
	public Map<String, String> getConfigurationParameters() {
		return configurationParameters;
	}

	public Map<String, String> getServiceParameters() {
		return serviceParameters;
	}

	public List<String> getImsiIds() {
		return imsiIds;
	}

	@Override
	public void isValid() throws MalformattedElementException {
		if (name == null) throw new MalformattedElementException("Instantiate VS request without VS instance name");
		if (description == null) throw new MalformattedElementException("Instantiate VS request without VS instance description");
		if (vsdId == null) throw new MalformattedElementException("Instantiate VS request without VSD ID");
		//if (tenantId == null) throw new MalformattedElementException("Instantiate VS request without tenant ID");

	}

}
