package it.nextworks.lcm.vs.interfaces.nfvo.messages;

public enum NfvoLcmNotificationType {
    LIFECYCLE_OPERATION_START,
    LIFECYCLE_OPERATION_RESULT
}
