package it.nextworks.nfvmano.catalogue.blueprints.exceptions;

public class AlreadyExistingEntityException extends Exception {

    public AlreadyExistingEntityException(String s){
        super(s);
    }
}
