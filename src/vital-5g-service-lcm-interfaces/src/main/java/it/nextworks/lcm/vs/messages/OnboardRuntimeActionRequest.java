package it.nextworks.lcm.vs.messages;

public class OnboardRuntimeActionRequest {

    private ScaleVsRequest scaleVsRequest;

    private String metricId;

    private RuntimeThresholdOperator thresholdOperator;

    private String metricValue;
    public enum RuntimeThresholdOperator {
        LE,
        LT,
        GE,
        GT,
        EQ
    }

    public OnboardRuntimeActionRequest() {
    }

    public OnboardRuntimeActionRequest(ScaleVsRequest scaleVsRequest, String metricId, RuntimeThresholdOperator thresholdOperator, String metricValue) {
        this.scaleVsRequest = scaleVsRequest;
        this.metricId = metricId;
        this.thresholdOperator = thresholdOperator;
        this.metricValue = metricValue;
    }

    public ScaleVsRequest getScaleVsRequest() {
        return scaleVsRequest;
    }

    public String getMetricId() {
        return metricId;
    }

    public RuntimeThresholdOperator getThresholdOperator() {
        return thresholdOperator;
    }

    public String getMetricValue() {
        return metricValue;
    }
}
