package it.nextworks.lcm.vs.messages;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ScaleVsSliceRequest extends ScaleVsRequest{

    private Map<String, String> sliceProfile = new HashMap<>();

    public ScaleVsSliceRequest(){
        super(null, ScaleVsType.SLICE_MODIFICATION);
    }

    public ScaleVsSliceRequest(UUID verticalServiceInstanceId, Map<String, String > sliceProfile) {
        super(verticalServiceInstanceId, ScaleVsType.SLICE_MODIFICATION );
        this.sliceProfile=sliceProfile;
    }
}
