package it.nextworks.lcm.vs.interfaces.nfvo.messages;

import it.nextworks.lcm.vs.elements.Testbed;

public class NetworkServiceStatusChangeNotification {

    private String nsiId;

    private Testbed testbed;

    private NetworkServiceStatusChange statusChange;


    public String getNsiId() {
        return nsiId;
    }

    public NetworkServiceStatusChange getStatusChange() {
        return statusChange;
    }

    public Testbed getTestbed() {
        return testbed;
    }

    public NetworkServiceStatusChangeNotification(String nsiId, Testbed testbed, NetworkServiceStatusChange statusChange) {
        this.nsiId = nsiId;
        this.testbed = testbed;
        this.statusChange = statusChange;
    }
}
