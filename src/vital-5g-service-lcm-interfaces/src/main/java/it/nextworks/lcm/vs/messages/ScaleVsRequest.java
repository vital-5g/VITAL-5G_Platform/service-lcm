package it.nextworks.lcm.vs.messages;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.UUID;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ScaleVsNSRequest.class, name = "NS_SCALE"),
        @JsonSubTypes.Type(value = ScaleVsSliceRequest.class, name = "SLICE_MODIFICATION"),

})
public class ScaleVsRequest {

    private UUID verticalServiceInstanceId;

    private ScaleVsType type;

    public ScaleVsRequest(UUID verticalServiceInstanceId, ScaleVsType type) {
        this.verticalServiceInstanceId = verticalServiceInstanceId;
        this.type = type;
    }

   public enum ScaleVsType{
        NS_SCALE,
        SLICE_MODIFICATION
    }

    public UUID getVerticalServiceInstanceId() {
        return verticalServiceInstanceId;
    }

    public ScaleVsType getType() {
        return type;
    }
}
