package it.nextworks.lcm.vs.elements;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class VerticalServiceInstanceInfo {
    @Id
    @GeneratedValue
    private UUID verticalServiceInstanceId;

    private String name;

    private AccessLevel accessLevel;
    private Testbed testbed;
    private VerticalServiceInstanceStatus status;
    private String owner;

    public VerticalServiceInstanceStatus getStatus() {
        return status;
    }

    public void setStatus(VerticalServiceInstanceStatus status) {
        this.status = status;
    }

    @ElementCollection(fetch=FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<UUID> activeExperiments = new ArrayList<>();


    @JsonIgnore
    @OneToOne(fetch= FetchType.EAGER, mappedBy = "verticalServiceInstanceInfo", cascade=CascadeType.ALL, orphanRemoval=true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private VerticalServiceInstance verticalServiceInstance;

    public VerticalServiceInstanceInfo() {
    }

    public VerticalServiceInstanceInfo(String name, Testbed testbed, AccessLevel accessLevel, String owner) {
        this.name = name;
        this.testbed = testbed;
        this.accessLevel=accessLevel;
        this.owner=owner;
    }

    public AccessLevel getAccessLevel() {
        return accessLevel;
    }

    public String getOwner() {
        return owner;
    }

    public UUID getVerticalServiceInstanceId() {
        return verticalServiceInstanceId;
    }

    public String getName() {
        return name;
    }

    public Testbed getTestbed() {
        return testbed;
    }

    public VerticalServiceInstance getVerticalServiceInstance() {
        return verticalServiceInstance;
    }

    public List<UUID> getActiveExperiments() {
        return activeExperiments;
    }

    public void addActiveExperiment(UUID experimentId){
        activeExperiments.add(experimentId);
    }

    public void removeActiveExperiment(UUID experimentId){
        if(activeExperiments.contains(experimentId)){
            activeExperiments.remove(experimentId);
        }
    }







}
