package it.nextworks.lcm.vs.messages;

public class VsExperimentAllocationRequest {

    private String experimentExecutionId;

    public VsExperimentAllocationRequest(String experimentExecutionId) {
        this.experimentExecutionId = experimentExecutionId;
    }

    public VsExperimentAllocationRequest() {
    }

    public String getExperimentExecutionId() {
        return experimentExecutionId;
    }
}
