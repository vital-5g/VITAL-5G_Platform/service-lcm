package it.nextworks.lcm.vs.interfaces.nfvo.messages;

public enum OperationStatus {

    SUCCESSFULLY_DONE,
    FAILED,
    PROCESSING;
}
