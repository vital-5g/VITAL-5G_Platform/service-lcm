package it.nextworks.lcm.vs.elements;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class ImsiEndpointSpecification {
    @Id
    @GeneratedValue
    private Long id;

    private String endpointId;
    @ElementCollection
    private List<String> imsiIds;

    @JsonIgnore
    @ManyToOne
    private VerticalServiceInstance verticalServiceInstance;

    public ImsiEndpointSpecification() {
    }

    public ImsiEndpointSpecification(String endpointId, List<String> imsiIds, VerticalServiceInstance verticalServiceInstance) {
        this.endpointId = endpointId;
        this.imsiIds = imsiIds;
        this.verticalServiceInstance = verticalServiceInstance;
    }

    public String getEndpointId() {
        return endpointId;
    }

    public List<String> getImsiIds() {
        return imsiIds;
    }

    public VerticalServiceInstance getVerticalServiceInstance() {
        return verticalServiceInstance;
    }
}
