package it.nextworks.lcm.vs.exceptions;

public class NotExistingEntityException extends Exception {
    public NotExistingEntityException(String s) {
        super(s);
    }
}
