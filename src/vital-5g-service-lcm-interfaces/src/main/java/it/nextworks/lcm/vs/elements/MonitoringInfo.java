package it.nextworks.lcm.vs.elements;

import javax.persistence.Embeddable;

@Embeddable
public class MonitoringInfo {

    private String networkMonitoringUrl;

    private String infrastructureMonitoringUrl;

    private String serviceMonitoringUrl;


    public MonitoringInfo() {
    }

    public MonitoringInfo(String infrastructureMonitoringUrl, String serviceMonitoringUrl, String networkMonitoringUrl) {
        this.infrastructureMonitoringUrl = infrastructureMonitoringUrl;
        this.serviceMonitoringUrl = serviceMonitoringUrl;
        this.networkMonitoringUrl=networkMonitoringUrl;
    }

    public String getInfrastructureMonitoringUrl() {
        return infrastructureMonitoringUrl;
    }

    public String getServiceMonitoringUrl() {
        return serviceMonitoringUrl;
    }

    public String getNetworkMonitoringUrl() {
        return networkMonitoringUrl;
    }
}
