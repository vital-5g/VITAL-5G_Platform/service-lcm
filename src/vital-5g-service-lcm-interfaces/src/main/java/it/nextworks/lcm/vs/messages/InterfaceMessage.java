package it.nextworks.lcm.vs.messages;

import it.nextworks.lcm.vs.exceptions.MalformattedElementException;

public interface InterfaceMessage {

    public void isValid() throws MalformattedElementException;
}
