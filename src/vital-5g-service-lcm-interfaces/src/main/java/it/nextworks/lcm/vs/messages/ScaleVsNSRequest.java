package it.nextworks.lcm.vs.messages;

import java.util.UUID;

public class ScaleVsNSRequest extends ScaleVsRequest{

    private int cpus =0;

    private int ram= 0;

    private String vnfdId;

    public ScaleVsNSRequest() {
        super(null, ScaleVsType.NS_SCALE);

    }

    public ScaleVsNSRequest(UUID vsiId, int cpus, int ram, String vnfdId) {
        super(vsiId, ScaleVsType.NS_SCALE);
        this.cpus = cpus;
        this.ram = ram;
        this.vnfdId = vnfdId;
    }

    public int getCpus() {
        return cpus;
    }

    public int getRam() {
        return ram;
    }

    public String getVnfdId() {
        return vnfdId;
    }
}
