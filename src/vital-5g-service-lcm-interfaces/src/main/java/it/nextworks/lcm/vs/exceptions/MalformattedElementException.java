package it.nextworks.lcm.vs.exceptions;

public class MalformattedElementException extends Exception{

    public MalformattedElementException(String message){
        super(message);
    }
}
