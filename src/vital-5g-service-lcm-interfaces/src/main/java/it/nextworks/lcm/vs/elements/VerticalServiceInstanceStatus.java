package it.nextworks.lcm.vs.elements;

public enum VerticalServiceInstanceStatus {
    CREATED,
    INSTANTIATING,
    INSTANTIATED,
    TERMINATING,
    TERMINATED,
    FAILED
}
