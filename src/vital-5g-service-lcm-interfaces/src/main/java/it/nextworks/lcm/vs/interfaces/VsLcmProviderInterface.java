/*
 * Copyright (c) 2019 Nextworks s.r.l
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.nextworks.lcm.vs.interfaces;

import it.nextworks.lcm.vs.elements.Testbed;
import it.nextworks.lcm.vs.elements.VerticalServiceInstance;
import it.nextworks.lcm.vs.elements.VerticalServiceInstanceInfo;
import it.nextworks.lcm.vs.exceptions.FailedOperationException;
import it.nextworks.lcm.vs.exceptions.MalformattedElementException;
import it.nextworks.lcm.vs.exceptions.NotExistingEntityException;
import it.nextworks.lcm.vs.exceptions.UnAuthorizedRequestException;
import it.nextworks.lcm.vs.messages.InstantiateVsRequest;
import it.nextworks.lcm.vs.messages.ScaleVsRequest;

import java.util.List;
import java.util.UUID;


/**
 * This interface allows to invoke lifecycle management 
 * requests for Vertical Services.
 * 
 * The following operations are defined for this interface:
 * 		Instantiate VS instance;
 * 		Query VS instance;
 * 		Terminate VS instance;
 * 		Modify VS instance.
 * 
 * This interface must be implemented by the Vertical Slicer core and 
 * it must be invoked by the plugins that manage its external interface 
 * (e.g. a REST Controller)
 * 
 * @author nextworks
 *
 */
public interface VsLcmProviderInterface {

	/**
	 * This method instantiates a new Vertical Service instance. 
	 * The method returns the ID of the vertical service instance and operates in asynchronous manner.
	 * In order to retrieve the actual status of the vertical service instance, the query method must be used.
	 *
     *
	 * @param request request to instantiate a new VS instance.
	 * @return The identifier of the VS instance.
	 * @throws NotExistingEntityException if the VSD does not exist.
	 * @throws FailedOperationException if the operation fails.
	 * @throws MalformattedElementException if the request is malformatted.
	 */
	public UUID instantiateVs(InstantiateVsRequest request)
            throws NotExistingEntityException, FailedOperationException, MalformattedElementException, UnAuthorizedRequestException;
	
	/**
	 * This method allows to query the Vertical Slicer for information about one or more Vertical Service instance(s). 
	 * 
	 * 
	 * @param tenant the Tenant of the vertical service instance
	 * @param testbed the Testbed of the vertical service instance.
	 * @param usecase of the vertical service instance.
	 * @param vsbId of the vertical service instance.
	 * @param vsdId of the vertical service instance.
	 * @return query response
	 * @throws NotExistingEntityException if the VS instance does not exist
	 * @throws FailedOperationException if the operation fails
	 * @throws MalformattedElementException if the request is malformatted

	 */
	public List<VerticalServiceInstanceInfo> queryVs(String tenant, Testbed testbed, String usecase, UUID vsbId, UUID vsdId)
			throws  NotExistingEntityException, FailedOperationException, MalformattedElementException;
	


	/**
	 * This method queries the IDs of all the VSs matching the request filter
	 *

	 * @param vsiId the id of the target vertical service instance
	 * @return List of Instances matching the request filter
	 * @throws NotExistingEntityException if the IDs are not found
	 * @throws FailedOperationException if the operation fails
	 * @throws MalformattedElementException if the request is malformatted
	 */
	public VerticalServiceInstance getVerticalServiceInstance(UUID vsiId)
            throws NotExistingEntityException, FailedOperationException, MalformattedElementException, UnAuthorizedRequestException;
	
	/**
	 * This method terminates an existing Vertical Service instance.
	 * This method operates in asynchronous manner.
	 * In order to retrieve the actual status of the vertical service instance, the query method must be used.
	 * 
	 * @throws NotExistingEntityException if the VS instance does not exist
	 * @throws FailedOperationException if the operation fails
	 * @throws MalformattedElementException if the request is malformatted

	 */
	public void terminateVs(UUID vsiId)
            throws NotExistingEntityException, FailedOperationException, MalformattedElementException, UnAuthorizedRequestException;


	/**
	 * This method deletes an existing Vertical Service instance.
	 * This method operates in asynchronous manner.
	 * In order to retrieve the actual status of the vertical service instance, the query method must be used.
	 *
	 * @throws NotExistingEntityException if the VS instance does not exist
	 * @throws FailedOperationException if the operation fails
	 * @throws MalformattedElementException if the request is malformatted

	 */
	public void deleteVs(UUID vsiId, boolean force)
			throws  NotExistingEntityException, FailedOperationException, MalformattedElementException;


	/**
	 * This method associates a Vertical Service instance to an experiment
	 * This method operates in asynchronous manner.
	 * In order to retrieve the actual status of the vertical service instance, the query method must be used.
	 * @param vsiId the id of the target vertical service instance
	 * @param experimentId the id of the experiment
	 * @throws NotExistingEntityException if the VS instance does not exist
	 * @throws FailedOperationException if the operation fails
	 * @throws MalformattedElementException if the request is malformatted

	 */
	public void useVs(UUID vsiId, UUID experimentId)
			throws  NotExistingEntityException, FailedOperationException, MalformattedElementException;



	/**
	 * This method removes the associatiaion between a Vertical Service instance and an experiment
	 * This method operates in asynchronous manner.
	 * In order to retrieve the actual status of the vertical service instance, the query method must be used.
	 * @param vsiId the id of the target vertical service instance
	 * @param experimentId the id of the experiment
	 * @throws NotExistingEntityException if the VS instance does not exist
	 * @throws FailedOperationException if the operation fails
	 * @throws MalformattedElementException if the request is malformatted

	 */
	public void releaseVs(UUID vsiId, UUID experimentId)
			throws  NotExistingEntityException, FailedOperationException, MalformattedElementException;


	public void scaleVs(ScaleVsRequest request) throws NotExistingEntityException, FailedOperationException, MalformattedElementException, UnAuthorizedRequestException;
}
