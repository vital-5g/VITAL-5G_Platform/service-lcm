package it.nextworks.lcm.vs.interfaces.nfvo.messages;

public enum NfvoLcmOperationType {
    NS_INSTANTIATION,
    NS_TERMINATION,
    NS_SCALING,
    DAY2,
    DAY2_CONFIG,
    DAY2_RUN,
    DAY2_CLEAN
}
