package it.nextworks.lcm.vs.elements;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.*;

@Entity
public class VerticalServiceInstance {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String description;
    private UUID vsdId;
    private String tenantId;

    @JsonIgnore
    @OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.REMOVE)
    @JoinColumn(name="vertical_service_instance_info")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private VerticalServiceInstanceInfo verticalServiceInstanceInfo;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(mappedBy = "verticalServiceInstance", cascade=CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<InfrastructureMetricRecord> infrastructureMetrics = new ArrayList<>();


    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(mappedBy = "verticalServiceInstance", cascade=CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<ApplicationMetricRecord> applicationMetrics = new ArrayList<>();

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private MonitoringInfo monitoringInfo;

    @ElementCollection(fetch = FetchType.EAGER)
    private Map<String, String> configurationParameters = new HashMap<>();
    @ElementCollection(fetch = FetchType.EAGER)
    private Map<String, String> serviceParameters = new HashMap<>();

    @ElementCollection(fetch = FetchType.EAGER)
    private Map<String, UUID> runtimeActions = new HashMap<>();
    @ElementCollection(fetch = FetchType.EAGER)
    private Map<String, String> serviceEndpoints = new HashMap<>();

    /*@JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(mappedBy = "verticalServiceInstance", cascade=CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<ImsiEndpointSpecification> imsiIds = new ArrayList<>();

     */

    @ElementCollection(fetch = FetchType.EAGER)
    private List<String> imsiIds = new ArrayList<>();

    private UUID verticalServiceInstanceId;

    private VerticalServiceInstanceStatus status;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String errorMsg;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String networkServiceInstanceId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String fiveGNetworkSliceId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String useCase;

    public String getFiveGNetworkSliceId() {
        return fiveGNetworkSliceId;
    }

    private Testbed testbed;

    public VerticalServiceInstance() {
    }

    public VerticalServiceInstance(String name,
                                   String description,
                                   UUID vsdId,
                                   String tenantId,
                                   Map<String, String> configurationParameters,
                                   Map<String, String> serviceParameters,
                                   List<String> imsiIds,
                                   VerticalServiceInstanceInfo vsiInfo,
                                   Testbed testbed) {
        this.name = name;
        this.description = description;
        this.vsdId = vsdId;
        this.tenantId = tenantId;
        this.configurationParameters = configurationParameters;
        this.serviceParameters = serviceParameters;
        if(imsiIds!=null) this.imsiIds = imsiIds;
        this.verticalServiceInstanceInfo = vsiInfo;
        if(vsiInfo!=null)
            this.verticalServiceInstanceId = vsiInfo.getVerticalServiceInstanceId();
        this.status = VerticalServiceInstanceStatus.CREATED;
        this.testbed=testbed;
    }

    public String getUseCase() {
        return useCase;
    }

    public MonitoringInfo getMonitoringInfo() {
        return monitoringInfo;
    }

    public void setMonitoringInfo(MonitoringInfo monitoringInfo) {
        this.monitoringInfo = monitoringInfo;
    }

    public VerticalServiceInstanceStatus getStatus() {
        return status;
    }

    public void setStatus(VerticalServiceInstanceStatus status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public UUID getVsdId() {
        return vsdId;
    }

    public Map<String, UUID> getRuntimeActions() {
        return runtimeActions;
    }

    public void setRuntimeActions(Map<String, UUID> runtimeActions) {
        this.runtimeActions = runtimeActions;
    }

    public String getTenantId() {
        return tenantId;
    }

    public Map<String, String> getConfigurationParameters() {
        return configurationParameters;
    }

    public Map<String, String> getServiceParameters() {
        return serviceParameters;
    }

    public List<String> getImsiIds() {
        return imsiIds;
    }

    public UUID getVerticalServiceInstanceId() {
        return verticalServiceInstanceId;
    }

    public Testbed getTestbed() {
        return testbed;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getNetworkServiceInstanceId() {
        return networkServiceInstanceId;
    }

    public void setNetworkServiceInstanceId(String networkServiceInstanceId) {
        this.networkServiceInstanceId = networkServiceInstanceId;
    }

    public List<InfrastructureMetricRecord> getInfrastructureMetrics() {
        return infrastructureMetrics;
    }

    public void setInfrastructureMetrics(List<InfrastructureMetricRecord> infrastructureMetrics) {
        this.infrastructureMetrics = infrastructureMetrics;
    }

    public List<ApplicationMetricRecord> getApplicationMetrics() {
        return applicationMetrics;
    }

    public void setApplicationMetrics(List<ApplicationMetricRecord> applicationMetrics) {
        this.applicationMetrics = applicationMetrics;
    }

    public Map<String, String> getServiceEndpoints() {
        return serviceEndpoints;
    }

    public void setServiceEndpoints(Map<String, String> serviceEndpoints) {
        this.serviceEndpoints = serviceEndpoints;
    }

    public VerticalServiceInstanceInfo getVerticalServiceInstanceInfo() {
        return verticalServiceInstanceInfo;
    }

    public void setFiveGNetworkSliceId(String fiveGNetworkSliceId) {
        this.fiveGNetworkSliceId = fiveGNetworkSliceId;
    }
}
