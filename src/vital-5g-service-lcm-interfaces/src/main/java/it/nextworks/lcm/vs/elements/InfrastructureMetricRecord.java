package it.nextworks.lcm.vs.elements;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class InfrastructureMetricRecord {

    @Id
    @GeneratedValue
    private Long id;
    private String metricId;
    private String topic;

    @JsonIgnore
    @ManyToOne
    private VerticalServiceInstance verticalServiceInstance;

    public String getTopic() {
        return topic;
    }

    public InfrastructureMetricRecord() {
    }

    public InfrastructureMetricRecord(String metricId, String topic, VerticalServiceInstance verticalServiceInstance) {
        this.metricId = metricId;
        this.topic = topic;
        this.verticalServiceInstance=verticalServiceInstance;
    }

    public String getMetricId() {
        return metricId;
    }

    public void setMetricId(String metricId) {
        this.metricId = metricId;
    }
}
