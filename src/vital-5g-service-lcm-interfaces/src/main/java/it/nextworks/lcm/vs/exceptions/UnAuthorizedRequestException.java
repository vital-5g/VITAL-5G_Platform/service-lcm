package it.nextworks.lcm.vs.exceptions;

public class UnAuthorizedRequestException extends Exception {

    public UnAuthorizedRequestException(String msg){
        super(msg);
    }
    public UnAuthorizedRequestException(Exception e){
        super(e);
    }


}
