/*
* Copyright 2018 Nextworks s.r.l.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package it.nextworks.lcm.vs.interfaces.nfvo;


import it.nextworks.lcm.vs.elements.Testbed;
import it.nextworks.lcm.vs.exceptions.FailedOperationException;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.CreateNsIdentifierRequestInternal;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.InstantiateNsRequestInternal;
import it.nextworks.lcm.vs.interfaces.nfvo.messages.OperationStatus;

import org.aspectj.weaver.ast.Test;

import java.util.Map;

public interface NsLcmProviderInterface {


    String createNetworkServiceIdentifier(CreateNsIdentifierRequestInternal request) throws FailedOperationException;

    String instantiateNetworkService(InstantiateNsRequestInternal request) throws FailedOperationException;

    OperationStatus getOperationStatus(String operationId, Testbed testbed) throws FailedOperationException;

    Map<String, String> getNetworkServiceInstanceEndpoints(String nsInstanceId, Testbed tesbed) throws FailedOperationException;

    String terminateNetworkService(String networkServiceInstanceId, Testbed testbed) throws FailedOperationException;

    String  executeDay2Action(String nsInstanceId, String actionId, String vnfProfileId, Map<String, String> inputParams, Testbed testbed) throws FailedOperationException;

    String getNfvoVmVimReference(String nsInstanceId, String vnfReference, Testbed testbed) throws FailedOperationException;
}
