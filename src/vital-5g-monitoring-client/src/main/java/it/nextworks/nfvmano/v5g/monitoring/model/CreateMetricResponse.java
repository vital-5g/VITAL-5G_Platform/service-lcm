package it.nextworks.nfvmano.v5g.monitoring.model;

import com.google.gson.annotations.SerializedName;

public class CreateMetricResponse {

    @SerializedName("topic")
    private String topic = null;

    @SerializedName("monitoringDashboardUrl")
    private String monitoringDashboardUrl = null;


    public CreateMetricResponse(String topic, String monitoringDashboardUrl) {
        this.topic = topic;
        this.monitoringDashboardUrl=monitoringDashboardUrl;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getMonitoringDashboardUrl() {
        return monitoringDashboardUrl;
    }

    public void setMonitoringDashboardUrl(String monitoringDashboardUrl) {
        this.monitoringDashboardUrl = monitoringDashboardUrl;
    }
}
